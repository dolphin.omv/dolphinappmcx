package org.test.client.mcopclient;

import org.test.client.mcopclient.ConstantsMCOP.CallEventExtras.CallTypeEnum;

public class CallEvent{

    public enum CallTypeValidEnum {
        NONE(0),
        AudioWithoutFloorCtrlPrivate(CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypePrivate.getValue()),
        AudioWithFloorCtrlPrivate(CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypePrivate.getValue()),
        AudioWithFloorCtrlPrivateEmergency (CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypePrivate.getValue()| CallTypeEnum.CallSubtypeEmergency.getValue()),
        AudioWithFloorCtrlPrearrangedGroupEmergency (CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypePrearrangedGroup.getValue()| CallTypeEnum.CallSubtypeEmergency.getValue()),
        AudioWithFloorCtrlPrearrangedGroupImminentPeril (CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypePrearrangedGroup.getValue()| CallTypeEnum.CallSubtypeImminentPeril.getValue()),
        AudioWithFloorCtrlPrearrangedGroup(CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypePrearrangedGroup.getValue()),
        AudioWithFloorCtrlChatGroupEmergency (CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypeChatGroup.getValue()| CallTypeEnum.CallSubtypeEmergency.getValue()),
        AudioWithFloorCtrlChatGroupImminentPeril (CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypeChatGroup.getValue()| CallTypeEnum.CallSubtypeImminentPeril.getValue()),
        AudioWithFloorCtrlChatGroup(CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypeChatGroup.getValue()),
        AudioWithFloorCtrlBroadcastpEmergency (CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypeBroadcastGroup.getValue()| CallTypeEnum.CallSubtypeEmergency.getValue()),
        AudioWithFloorCtrlBroadcastImminentPeril (CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypeBroadcastGroup.getValue()| CallTypeEnum.CallSubtypeImminentPeril.getValue()),
        AudioWithFloorCtrlBroadcast(CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypeBroadcastGroup.getValue()),
        AudioWithFloorCtrlFirstToAnswer(CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypeFirstToAnswer.getValue()),
        AudioWithFloorCtrlPrivateCallCallback(CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypePrivateCallCallback.getValue()),
        AudioWithFloorCtrlRemoteAmbientListening(CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypeRemoteAmbientListening.getValue()),
        AudioWithFloorCtrlLocalAmbientListening(CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypeLocalAmbientListening.getValue()),
        VideoAudioWithFloorCtrlPrivate(CallTypeEnum.CallSubtypeVideo.getValue() | CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypePrivate.getValue()),
        VideoAudioWithFloorCtrlPrivateEmergency (CallTypeEnum.CallSubtypeVideo.getValue() |CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypePrivate.getValue()| CallTypeEnum.CallSubtypeEmergency.getValue()),
        VideoAudioWithFloorCtrlPrearrangedGroupEmergency (CallTypeEnum.CallSubtypeVideo.getValue() |CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypePrearrangedGroup.getValue()| CallTypeEnum.CallSubtypeEmergency.getValue()),
        VideoAudioWithFloorCtrlPrearrangedGroupImminentPeril (CallTypeEnum.CallSubtypeVideo.getValue() |CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypePrearrangedGroup.getValue()| CallTypeEnum.CallSubtypeImminentPeril.getValue()),
        VideoAudioWithFloorCtrlPrearrangedGroup(CallTypeEnum.CallSubtypeVideo.getValue() |CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypePrearrangedGroup.getValue()),
        VideoAudioWithFloorCtrlChatGroupEmergency (CallTypeEnum.CallSubtypeVideo.getValue() |CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypeChatGroup.getValue()| CallTypeEnum.CallSubtypeEmergency.getValue()),
        VideoAudioWithFloorCtrlChatGroupImminentPeril (CallTypeEnum.CallSubtypeVideo.getValue() |CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypeChatGroup.getValue()| CallTypeEnum.CallSubtypeImminentPeril.getValue()),
        VideoAudioWithFloorCtrlChatGroup(CallTypeEnum.CallSubtypeVideo.getValue() |CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypeChatGroup.getValue()),
        VideoAudioWithFloorCtrlBroadcastpEmergency (CallTypeEnum.CallSubtypeVideo.getValue() |CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypeBroadcastGroup.getValue()| CallTypeEnum.CallSubtypeEmergency.getValue()),
        VideoAudioWithFloorCtrlBroadcastImminentPeril (CallTypeEnum.CallSubtypeVideo.getValue() |CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypeBroadcastGroup.getValue()| CallTypeEnum.CallSubtypeImminentPeril.getValue()),
        VideoAudioWithFloorCtrlBroadcast(CallTypeEnum.CallSubtypeVideo.getValue() |CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypeBroadcastGroup.getValue()),
        VideoAudioWithFloorCtrlFirstToAnswer(CallTypeEnum.CallSubtypeVideo.getValue() |CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypeFirstToAnswer.getValue()),
        VideoAudioWithFloorCtrlPrivateCallCallback(CallTypeEnum.CallSubtypeVideo.getValue() |CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypePrivateCallCallback.getValue()),
        VideoAudioWithFloorCtrlRemoteAmbientListening(CallTypeEnum.CallSubtypeVideo.getValue() |CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypeRemoteAmbientListening.getValue()),
        VideoAudioWithFloorCtrlLocalAmbientListening(CallTypeEnum.CallSubtypeVideo.getValue() | CallTypeEnum.CallSubtypeAudio.getValue() | CallTypeEnum.CallSubtypeWithCtrlMech.getValue()| CallTypeEnum.CallSubtypeLocalAmbientListening.getValue());
        private int code;
        CallTypeValidEnum(int code) {
            this.code = code;
        }
        public int getValue() {
            return code;
        }
    }

    public static CallEvent.CallTypeValidEnum validationCallType(int type){
        if(type<=0)return null;
        for(CallEvent.CallTypeValidEnum data:CallEvent.CallTypeValidEnum.values()){
            if(data.getValue()==type) {
                return data;
            }
        }
        return null;
    }
}