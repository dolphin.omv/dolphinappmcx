/*
  Copyright (C) 2020, University of the Basque Country (UPV/EHU)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package org.test.client.mcopclient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * MCOP MCPTT SDK
 * Constants used on AIDLs
 *
 * @version 0.1
 */
public class ConstantsMCOP {

    public static final int NO_ERROR = 0;

    /**
     * <h2>Key Access to the values of the packet names of Iapi plugins.</h2>
     * <p>This values are passed as Strings in SDK binding intent</p>
     */
    public static final String CONNECTIVITY_PLUGIN_PACKAGE_ID = "org.mcopenplatform.muoapi.ConstantsMCOP" + ".CONNECTIVITY_PLUGIN_PACKAGE_ID";
    public static final String CONNECTIVITY_PLUGIN_SERVICE_ID = "org.mcopenplatform.muoapi.ConstantsMCOP" + ".CONNECTIVITY_PLUGIN_SERVICE_ID";
    public static final String SIM_PLUGIN_PACKAGE_ID = "org.mcopenplatform.muoapi.ConstantsMCOP" + ".SIM_PLUGIN_PACKAGE_ID";
    public static final String SIM_PLUGIN_SERVICE_ID = "org.mcopenplatform.muoapi.ConstantsMCOP" + ".SIM_PLUGIN_SERVICE_ID";
    public static final String CONFIGURATION_PLUGIN_PACKAGE_ID = "org.mcopenplatform.muoapi.ConstantsMCOP" + ".CONFIGURATION_PLUGIN_PACKAGE_ID";
    public static final String CONFIGURATION_PLUGIN_SERVICE_ID = "org.mcopenplatform.muoapi.ConstantsMCOP" + ".CONFIGURATION_PLUGIN_SERVICE_ID";
    public static final String MBMS_PLUGIN_PACKAGE_ID = "org.mcopenplatform.muoapi.ConstantsMCOP" + ".MBMS_PLUGIN_PACKAGE_ID";
    public static final String MBMS_PLUGIN_SERVICE_ID = "org.mcopenplatform.muoapi.ConstantsMCOP" + ".MBMS_PLUGIN_SERVICE_ID";

    /**
     * <h2>Key Access to the values of action for delete token</h2>
     * <p>This values are passed as boolean in SDK binding intent</p>
     */
    public static final String TOKEN_DELETE_SERVICE = "org.mcopenplatform.muoapi.LoginEventExtras.ConstantsMCOP" + ".TOKEN_DELETE_SERVICE";

    /**
     * <h2> Flag indicating that the affiliation subscription operation will be manually handled.</h2>
     * <p>This flag is required for the following method to be used {@link IMCOPsdk#affiliationSubscriptionOperation(String, boolean)}</p>
     * <p>This values are passed as boolean in SDK binding intent</p>
     */
    public static final String MANUAL_AFFILIATION_SUBSCRIPTION_CONTROL = "org.mcopenplatform.muoapi.ConstantsMCOP" + ".MANUAL_AFFILIATION_SUBSCRIPTION_CONTROL";

    /**
     * @brief Manages the events regarding registration
     *
     * <h1>Key Access to the values of {@link ConstantsMCOP.ActionsCallBack#registrationEvent} event types.</h1>
     * <p>This class contains all the answers to the actions of methods {@link IMCOPsdk#login()} , {@link IMCOPsdk#logout()} , {@link IMCOPsdk#authorizeUser(String url)} </p>
     */

    public static class RegistrationEventExtras{

        /**
         * <h2>Key Access to the registration event type. See {@link org.mcopenplatform.muoapi.ConstantsMCOP.RegistrationEventExtras.RegistrationEventTypeEnum} for all possible values.</h2>
         * <p>Response: Integer type value </p>
         */
        public static final String EVENT_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.RegistrationEventExtras" + ".EVENT_TYPE";

        /**
         * <h2>Key Access to the uri to request the user authentication to a third party.</h2>
         * <p>This data is received once the call to the method {@link IMCOPsdk#login()} ()} is made.</p>
         * <p>Response: URI format string</p>
         */
        public static final String REQUEST_URI = "org.mcopenplatform.muoapi.ConstantsMCOP.RegistrationEventExtras" + ".REQUEST_URI";
        /**
         * <h2>Key Access to the uri answered by the third party authentication agent.</h2>
         * <h3>Indicates when the third party authentication ends, and the method {@link org.mcopenplatform.muoapi.IMCOPsdk#authorizeUser(String)} can be called with the authentication response.</h3>
         * <p>This data is received once the call to the method {@link IMCOPsdk#login()} ()} is made.</p>
         * <p>Response: URI format string</p>
         */
        public static final String REDIRECT_URI = "org.mcopenplatform.muoapi.ConstantsMCOP.RegistrationEventExtras" + ".REDIRECT_URI";


        /**
         * <h2>Key Access to the ID that unequivocally identifies the user in MCPTT service once authenticated.</h2>
         * <p>Response: String with the ID that unequivocally identifies the user in the MCPTT service.</p>
         * <p>The registration event types in which this tag can be present are: </br> </p>
         * <p>RegisterEventSuccess {@link org.mcopenplatform.muoapi.ConstantsMCOP.RegistrationEventExtras.RegistrationEventTypeEnum#RegisterEventSuccess}</p>
         */
        public static final String MCPTT_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.RegistrationEventExtras" + ".MCPTT_ID";

        /**
         * <h2>Key Access to the ID that unequivocally identifies the user in MCVideo service once authenticated.</h2>
         * <p>Response: String with the ID that unequivocally identifies the user in the MCVideo service.</p>
         * <p>The registration event types in which this tag can be present are: </br> </p>
         * <p>RegisterEventSuccess {@link org.mcopenplatform.muoapi.ConstantsMCOP.RegistrationEventExtras.RegistrationEventTypeEnum#RegisterEventSuccess}</p>
         */
        public static final String MCVIDEO_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.RegistrationEventExtras" + ".MCVIDEO_ID";

        /**
         * <h2>Key Access to the ID that unequivocally identifies the user in MCData service once authenticated.</h2>
         * <p>Response: String with the ID that unequivocally identifies the user in the MCData service.</p>
         * <p>The registration event types in which this tag can be present are: </br> </p>
         * <p>RegisterEventSuccess {@link org.mcopenplatform.muoapi.ConstantsMCOP.RegistrationEventExtras.RegistrationEventTypeEnum#RegisterEventSuccess}</p>
         */
        public static final String MCDATA_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.RegistrationEventExtras" + ".MCDATA_ID";

        /**
         * <h2>Key Access to identify the user with a human-readable alias once authenticated.</h2>
         * <p>Response: String that identifies the user with a human-readable alias, more pleasant than a MCX identifier.</p>
         * <p>The registration event types in which this tag can be present are: </br> </p>
         * <p>RegisterEventSuccess {@link org.mcopenplatform.muoapi.ConstantsMCOP.RegistrationEventExtras.RegistrationEventTypeEnum#RegisterEventSuccess}</p>
         */
        public static final String DISPLAY_NAME = "org.mcopenplatform.muoapi.ConstantsMCOP.RegistrationEventExtras" + ".DISPLAY_NAME";

        /**
         * <h2>Key Access to the error codes associated to the registration events.</h2>
         * <p>Response: Integer indicating the error code.</p>
         * <p>The registration event types in which this tag can be present are: </br> </p>
         * <p>RegisterEventError {@link org.mcopenplatform.muoapi.ConstantsMCOP.RegistrationEventExtras.RegistrationEventTypeEnum#RegisterEventError}</p>
         * <p>Values:</p>
         * <p>0 means no errors.</p>
         * <p>1XX: xMS Errors:</p>
         * <p>  101 Unknown GMS address</p>
         * <p>  102 Unknown CMS address</p>
         * <p>  103 Unknown KMS address</p>
         * <p>  104 Incorrect answer from GMS</p>
         * <p>  105 Incorrect answer from CMS</p>
         * <p>  106 Incorrect answer from KMS</p>
         * <p>  107 Error while performing CMS procedures</p>
         * <p>2XX: IMS or SIM Authentication Errors:</p>
         * <p>  201 IMS registration error</p>
         * <p>  202 IMS authentication error</p>
         * <p>  203 Synchronization error between SIM and IMS</p>
         * <p>  204 SIM access error</p>
         * <p>  205 It is not possible to login because the customer is logged in right now</p>
         * <p>  206 Authentication URL is not valid</p>
         * <p>  207 There was an error in the registration process</p>
         * <p>  208 Error while performing authentication procedures</p>
         * <p>3XX: IDMS Errors:</p>
         * <p>UnregisterEventError {@link org.mcopenplatform.muoapi.ConstantsMCOP.RegistrationEventExtras.RegistrationEventTypeEnum#UnregisterEventError}</p>
         * <p>Values:</p>
         * <p>  0 means no errors.</p>
         * <p>  205 It is not possible performing logging out procedure because the client is not logged right now</p>
         * <p>  207 Error while performing logging out procedure </p>
         *
         */
        public static final String ERROR_CODE = "org.mcopenplatform.muoapi.ConstantsMCOP.RegistrationEventExtras" + ".ERROR_CODE";

        /**
         * <h2>Key Access to error string associated to the registration events.</h2>
         * <p>Response: String describing the error.</p>
         */
        public static final String ERROR_STRING = "org.mcopenplatform.muoapi.ConstantsMCOP.RegistrationEventExtras" + ".ERROR_STRING";

        /**
         * @enum RegistrationEventTypeEnum
         * @brief Enumeration of all possible registration events
         *
         * <h2>Defines the different events that can occur related to MCS service registration.</h2>
         */
        public enum RegistrationEventTypeEnum {

            None(0x00), /* !< Invalid registration event type */
            RegisterEventError(0x01), /* !< Registration error event. */
            RegisterEventSuccess(0x02), /*!< Successful registration. This event occurs when the end of the login procedure is reached */
            RegisterEventIdmsAuth(0x03), /*!< IDMS authentication. This event occurs during login procedure.
             * When the SDK sends this event, it will stop the login procedure until the upper client layers execute either the function to authorize the user or logouts. */
            UnregisterEventError(0x04), /*!< Unregistration error event.*/
            UnregisterEventSuccess(0x05); /*!< Successful unregistration. This event occurs when the end of the logout procedure is reached*/

            private int code;

            RegistrationEventTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (RegistrationEventExtras.RegistrationEventTypeEnum pageType : RegistrationEventExtras.RegistrationEventTypeEnum.values()) {
                    map.put(pageType.code, pageType);
                }
            }

            public static RegistrationEventExtras.RegistrationEventTypeEnum fromInt(int pageType) {
                return (RegistrationEventExtras.RegistrationEventTypeEnum) map.get(pageType);
            }
        }
    }

    /**
     * @brief Manages the events regarding call establishment
     *
     * <h1>Key Access to the values of {@link ConstantsMCOP.ActionsCallBack#callEvent} event types</h1>
     * <p>This class contains all the answers to the actions of methods {@link IMCOPsdk#makeCall(String[], int, String, Map)} ()} </p>
     */
    public static class CallEventExtras {

        /**
         * <h2> Key Access to the call event type. See {@link org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras.CallEventTypeEnum} for all possible values.</h2>
         * <p>Integer type value </p>
         * <p>This parameter is included in all call type events {@link ConstantsMCOP.CallEventExtras.CallEventTypeEnum}</p>
         */
        public static final String EVENT_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras" + ".EVENT_TYPE";

        /**
         * <h2>Key Access to the call session identifier. This identifier is unique for each call.</h2>
         * <p>Response: String type value </p>
         * <p>This parameter is included in all call type events {@link ConstantsMCOP.CallEventExtras.CallEventTypeEnum}</p>
         */
        public static final String SESSION_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras" + ".SESSION_ID";

        /**
         * <h2>Key Access to the call type. See {@link org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras.CallTypeEnum} for all possible values.</h2>
         * <p>Response: Integer type value </p>
         * <p>The call event types in which this tag can be present are: </br> </p>
         * <p>CallEventIncoming {@link org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras.CallEventTypeEnum#CallEventIncoming} </br> </p>
         * <p>CallEventRinging {@link org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras.CallEventTypeEnum#CallEventRinging} </br> </p>
         * <p>CallEventConnected {@link org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras.CallEventTypeEnum#CallEventConnected} </br> </p>
         * <p>CallEventUpdated {@link org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras.CallEventTypeEnum#CallEventUpdated} </p>
         */
        public static final String CALL_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras" + ".CALL_TYPE";

        /**
         * <h2>Key Access to the MCS Identity of the caller user. Depending on whether the call is MCPTT or MCVIDEO, this tag will contain the MCPTT ID or MCVIDEO ID, respectively.</h2>
         * <p>Response: String type value representing the identifier </p>
         * <p>The call event types in which this tag can be present are: </br> </p>
         * <p>CallEventIncoming {@link org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras.CallEventTypeEnum#CallEventIncoming}</br> </p>
         * <p>CallEventConnected {@link org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras.CallEventTypeEnum#CallEventConnected}</p>
         */
        public static final String CALLER_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras" + ".CALLER_ID";

        /**
         * <h2>Key Access to the current functional alias of the caller user.</h2>
         * <p>Response: String type value representing the identifier </p>
         * <p>The call event types in which this tag can be present are: </br> </p>
         * <p>CallEventIncoming {@link org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras.CallEventTypeEnum#CallEventIncoming}</br> </p>
         * <p>CallEventConnected {@link org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras.CallEventTypeEnum#CallEventConnected}</p>
         */
        public static final String CALLER_FUNCTIONAL_ALIAS = "org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras" + ".CALLER_FUNCTIONAL_ALIAS";

        /**
         * <h2>Key Access to the caller group identity </h2>
         * <p>Response: String type value representing the identifier </p>
         * <p>The call event types in which this tag can be present are: </br> </p>
         * <p>CallEventIncoming {@link org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras.CallEventTypeEnum#CallEventIncoming}</br> </p>
         * <p>CallEventConnected {@link org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras.CallEventTypeEnum#CallEventConnected}</p>
         */
        public static final String GROUP_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras" + ".GROUP_ID";

        /**
         * <h2>Key Access to the error codes associated to the call events.</h2>
         * <p>Response: Integer indicating the error code.</p>
         * <p>The call event types in which this tag can be present are: </br> </p>
         * <p>CallEventError {@link org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras.CallEventTypeEnum#CallEventError}</br> </p>
         * <p>Values:</p>
         * <p>Code	Explanatory text	Description</p>
         * <p>103	maximum simultaneous MCPTT group calls reached	The number of maximum simultaneous MCPTT group calls supported for the MCPTT user has been exceeded.</p>
         * <p>104	isfocus not assigned	A controlling MCPTT function has not been assigned to the MCPTT session.</p>
         * <p>105	subscription not allowed in a broadcast group call	Subscription to the conference event package rejected during a group call initiated as a broadcast group call.</p>
         * <p>106	user not authorised to join chat group	The MCPTT user is not authorised to join this chat group.</p>
         * <p>107	user not authorised to make private calls	The MCPTT user is not authorised to make private calls.</p>
         * <p>108	user not authorised to make chat group calls	The MCPTT user is not authorised to make chat group calls.</p>
         * <p>109	user not authorised to make prearranged group calls	The MCPTT user is not authorised to make group calls to a prearranged group.</p>
         * <p>110	user declined the call invitation	The MCPTT user declined to accept the call.</p>
         * <p>111	group call proceeded without all required group members	The required members of the group did not respond within the acknowledged call time, but the call still went ahead.</p>
         * <p>112	group call abandoned due to required group members not part of the group session	The group call was abandoned, as the required members of the group did not respond within the acknowledged call time.</p>
         * <p>116	user is not part of the MCPTT group	The group exists on the group management server but the requesting user is not part of this group.</p>
         * <p>117	the group identity indicated in the request is a prearranged group	The group id that is indicated in the request is for a prearranged group, but did not match the request from the MCPTT user.</p>
         * <p>118	the group identity indicated in the request is a chat group	The group id that is indicated in the request is for a chat group, but did not match the request from the MCPTT user.</p>
         * <p>119	user is not authorised to initiate the group call	The MCPTT user identified by the MCPTT ID is not authorised to initiate the group call.</p>
         * <p>120	user is not affiliated to this group	The MCPTT user is not affiliated to the group.</p>
         * <p>121	user is not authorised to join the group call	The MCPTT user identified by the MCPTT ID is not authorised to join the group call.</p>
         * <p>122	too many participants	The group call has reached its maximum number of participants.</p>
         * <p>123(INFO)	MCPTT session already exists	Inform the MCPTT user that the group call is currently ongoing.</p>
         * <p>124	maximum number of private calls reached	The maximum number of private calls allowed at the MCPTT server for the MCPTT user has been reached.</p>
         * <p>127	user not authorised to be called in private call	The called MCPTT user is not allowed to be part of a private call.</p>
         * <p>144	user not authorised to call this particular user	The calling user is not authorised to call this particular called user.</p>
         * <p>145	unable to determine called party	The participating function was unable to determine the called party from the information received in the SIP request.</p>
         * <p>147(INFO)	user is authorized to initiate a temporary group call	The non-controlling MCPTT function has authorized a request from the controlling MCPTT function to authorize a user to initiate an temporary group session.</p>
         * <p>148(INFO)	MCPTT group is regrouped	The MCPTT group hosted by a non-controlling MCPTT function is part of a temporary group session as the result of the group regroup function.</p>
         * <p>151	user not authorised to make a private call call-back request	The MCPTT user is not authorised to make a private call call-back request.</p>
         * <p>152	user not authorised to make a private call call-back cancel request	The MCPTT user is not authorised to make a private call call-back cancel request.</p>
         * <p>153	user not authorised to call any of the users requested in the first-to-answer call	All users that were invited in the first-to-answer call cannot be involved in a private call with the inviting user.</p>
         * <p>154	user not authorised to make ambient listening call	The MCPTT user is not authorised to make an ambient listening call.</p>
         * <p>156	user not authorised to originate a first-to-answer call	The MCPTT user is not authorised to make a first-to-answer call.</p>
         *
         * <p>401   Invalid call type.</p>
         * <p>402   Invalid action.</p>
         * <p>403   Invalid UserID in MakeCall. The UserID is empty, or does not have URI format</p>
         * <p>404   The call can not be hung because is not made</p>
         * <p>405   The call cannot be accepted because it has not received any call for that sessionID</p>
         * <p>406   The sessionID could not be created</p>
         * <p>407   The call cannot be made because this unregistered</p>
         * <p>408   User destination is not available at the moment</p>
         * <p>409   Undefined signal error</p>
         * <p>410   The destination is busy</p>
         */
        public static final String ERROR_CODE = "org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras" + ".ERROR_CODE";

        /**
         * <h2>Key Access to error string associated to the call events.</h2>
         * <p>Response: String describing the error.</p>
         */
        public static final String ERROR_STRING = "org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras" + ".ERROR_STRING";

        /**
         * <h2> Key Access to the call optional answer mode tag.</h2>
         * <p> Indicates the answer mode in which the application should answer. <p>
         * <p> It can be used in call requests indicating the desired answer mode of the called user {@link IMCOPsdk#makeCall(String[], int, String, Map)} </p>
         * <p> Contains a String indicating if the answer mode is "manual" or "auto" </p>
         */
        public static final String TAG_CALL_ANSWER_MODE_KEY = "org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras" + ".TAG_CALL_ANSWER_MODE_KEY";

        /**
         * <h2> Key Access to the call optional emergency alert activation tag.</h2>
         * <p> Indicates if the emergency alert wants to be activated or not, while starting the call. <p>
         * <p> It can be used in call requests indicating emergency alert activation for the called group {@link IMCOPsdk#makeCall(String[], int, String, Map)}</p>
         * <p> Contains a boolean value. True means activate </p>
         * @note This tag can only be used with group calls.
         */
        public static final String TAG_CALL_EMERG_ALERT_KEY = "org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras" + ".TAG_CALL_EMERG_ALERT_KEY";

        /**
         * <h2> Key Access to the call optional microphone mute tag.</h2>
         * <p> Indicates if it is wanted the microphone to be muted or not when a new call is established. <p>
         * <p> It can be used in call requests indicating that the microphone wants to be muted {@link IMCOPsdk#makeCall(String[], int, String, Map)} and {@link IMCOPsdk#acceptCall(String, Map)}</p>
         * <p> Contains a boolean value. True means activate </p>
         */
        public static final String TAG_CALL_MIC_MUTE_KEY = "org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras" + ".TAG_CALL_MIC_MUTE_KEY";

        /**
         * <h2> Key Access to the call optional speaker mute tag.</h2>
         * <p> Indicates if it is wanted the speaker to be muted or not when a new call is established. <p>
         * <p> It can be used in call requests indicating that the speaker wants to be muted {@link IMCOPsdk#makeCall(String[], int, String, Map)} and {@link IMCOPsdk#acceptCall(String, Map)} </p>
         * <p> Contains a boolean value. True means activate </p>
         */
        public static final String TAG_CALL_SPEAKER_MUTE_KEY = "org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras" + ".TAG_CALL_SPEAKER_MUTE_KEY";

        /**
         * <h2> Key Access to the call optional implicit request tag.</h2>
         * <p> Indicates if it is wanted to request implicitly the floor control token when a new call is established. <p>
         * <p> It can be used in call requests {@link IMCOPsdk#makeCall(String[], int, String, Map)} </p>
         * <p> It is a boolean value. True means activate. </p>
         */
        public static final String TAG_CALL_IMPLICIT_REQUEST_KEY = "org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras" + ".TAG_CALL_IMPLICIT_REQUEST_KEY";

        /**
         * <h2> Key Access to the call optional implicit granted tag.</h2>
         * <p> Indicates if it is wanted to grant implicitly the floor control token when a new call is established. <p>
         * <p> It can be used in call requests {@link IMCOPsdk#makeCall(String[], int, String, Map)} </p>
         * <p> It is a boolean value. True means activate. </p>
         */
        public static final String TAG_CALL_IMPLICIT_GRANTED_KEY = "org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras" + ".TAG_CALL_IMPLICIT_GRANTED_KEY";


        /**
         * @enum CallEventTypeEnum
         * @brief Enumeration of all possible call events
         *
         * <h2>Defines the different events that can occur related to a call or communication</h2>
         */
        public enum CallEventTypeEnum {
            CallEventNone(0x00), /*!< Invalid call event type */
            CallEventError(0x01), /*!< Error event */
            CallEventIncoming(0x02), /*!< Indicates an incoming call */
            CallEventCalling(0x03), /*!< Indicates that an outgoing call is being routed */
            CallEventRinging(0x04), /*!< Indicates that the call is ringing at the far end of the communication. This event will only occur with manual commencement private calls */
            CallEventConnected(0x05), /*!< Indicates that the call has been established */
            CallEventReleased(0x06), /*!< Indicates that the call has been released */
            CallEventUpdated(0x07); /*!< Indicates that the call state has been updated (e.g. upgrade/downgrade to/from emergency priority)*/

            private int code;

            CallEventTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (CallEventTypeEnum callEventTypeEnum : CallEventTypeEnum.values()) {
                    map.put(callEventTypeEnum.code, callEventTypeEnum);
                }
            }

            public static CallEventTypeEnum fromInt(int setCallEventTypeEnum) {
                return (CallEventTypeEnum) map.get(setCallEventTypeEnum);
            }
        }

        /*!
         * @enum CallSubtypeEnum
         * @brief Enumeration of all possible call subtypes
         *
         * <h2>Defines the different call subtypes that can be combined to form the call types.</h2>
         */
        public enum CallTypeEnum {

            CallSubtypeNone(0x00), /*!< Invalid call subtype */
            CallSubtypeAudio(0x01 << 0), /*!< Audio call indicator */
            CallSubtypeVideo(0x01 << 1), /*!< Video call indicator. This subtype only indicates that the call contains video,
             * not video and audio  */
            CallSubtypeWithCtrlMech(0x01 << 2), /*!< Media stream control mechanism indicator (e.g floor control or transmission control).
             * Group calls must always have this indicator set */
            CallSubtypePrivate(0x01 << 3), /*!< Private call indicator */
            CallSubtypeBroadcastGroup(0x01 << 4), /*!< Broadcast group call indicator */
            CallSubtypePrearrangedGroup(0x01 << 5), /*!< Pre-arranged group call indicator */
            CallSubtypeChatGroup(0x01 << 6), /*!< Chat group call indicator*/
            CallSubtypeRemoteAmbientListening(0x01 << 7),/*!< Remotely-initiated ambient-listening call indicator*/
            CallSubtypeLocalAmbientListening(0x01 << 8), /*!< Locally-initated ambient-listening call indicator */
            CallSubtypeFirstToAnswer(0x01 << 9),/*!< First-To-Answer call indicator. Several users are called but only the first responder gets to establish an actual call */
            CallSubtypePrivateCallCallback(0x01 << 10), /*!<  Private call call-back request indicator */
            CallSubtypeCancellationRequest(0x01 << 11),/*!< Cancellation request indicator.
             * This subtype could be combined with other subtypes to indicate that an already
             * performed request wants to be cancelled (e.g Private call call-back request cancellation) */
            CallSubtypeEmergency(0x01 << 12),  /*!< Emergency call priority indicator */
            CallSubtypeImminentPeril(0x01 << 13);  /*!< Imminent peril call priority indicator. This call priority is only available for group calls.*/

            private int code;

            CallTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            public static List<CallTypeEnum> getListCallType(int num) {
                ArrayList<CallTypeEnum> callTypesEnum = new ArrayList<>();
                for (CallTypeEnum callTypeEnum : CallTypeEnum.values()) {
                    if ((num & callTypeEnum.getValue()) == callTypeEnum.getValue()) {
                        callTypesEnum.add(callTypeEnum);
                    }
                }

                return callTypesEnum;
            }

            public static Map<CallTypeEnum, Integer> getMapCallType(int num) {
                Map<CallTypeEnum, Integer> callTypesEnum = new HashMap<>();
                for (CallTypeEnum callTypeEnum : CallTypeEnum.values()) {
                    if ((num & callTypeEnum.getValue()) == callTypeEnum.getValue()) {
                        callTypesEnum.put(callTypeEnum, callTypeEnum.getValue());
                    }
                }
                return callTypesEnum;
            }

            public static int getValue(List<CallTypeEnum> callTypeEnums) {
                int value = CallSubtypeNone.getValue();
                for (CallTypeEnum callTypeEnum : callTypeEnums) {
                    if (callTypeEnum != null) {
                        value += callTypeEnum.getValue();
                    }
                }

                return value;
            }
        }
    }

    /**
     * @brief MCData management related event. Manages the events related to MCDATA
     *
     * <h1>Key Access to the values of {@link ConstantsMCOP.ActionsCallBack#mcdataEvent} event types </h1>
     * <p>This class contains all the answers to the actions of methods {@link IMCOPsdk#sendSDS(String, String[], int[], long, String, int)}
     * , {@link IMCOPsdk#createConversation(String, int, int)} , {@link IMCOPsdk#sendSDS(String, String[], int[], long, String, int)} </p>
     */
    public static class MCDataEventExtras {

        /**
         * <h2>Key Access to the mcdata event type. See {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum} for all possible values.</h2>
         * <p>Response: Integer type value </p>
         */
        public static final String EVENT_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".EVENT_TYPE";

        /**
         * <h2>Key Access to the SDS Payload list related to a SDS message </h2>
         * <p>Response: Payload list (String [])</p>
         * <p> The Payload list is retrieved as a result of an incoming SDS message. </p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsReceived {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsReceived}</p>
         */
        public static final String SDS_PAYLOAD_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".SDS_PAYLOAD_LIST";

        /**
         * <h2>Key Access to the list of SDS Payload list (related to a SDS message) </h2>
         * <p>Response: List of SDS Payload list (String [][])</p>
         * <p> The list of SDS payload list is retrieved as a result of a message history retrieving request. As each SDS Message includes a SDS payload list, multiple SDS messages contain a list of SDS payload list. </p>
         * <p>This is use in response to method {@link IMCOPsdk#getAllSDS(String, String, Map)}  if conversationID and recipientID parameters are not null</p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsMessageListHistory {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsMessageListHistory}</p>
         */
        public static final String SDS_PAYLOAD_LIST_OF_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".SDS_PAYLOAD_LIST_OF_LIST";

        /**
         * <h2>Key Access to the SDS Payload list type related to a SDS message </h2>
         * <p>Response: Payload type list (String [])</p>
         * <p> The Payload list type is retrieved as a result of an incoming SDS message. Indicates the type of each payload associate to a SDS Message </p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsReceived {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsReceived}</p>
         */
        public static final String SDS_PAYLOAD_LIST_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".SDS_PAYLOAD_LIST_TYPE";

        /**
         * <h2>Key Access to the list of SDS Payload type list (related to a SDS message) </h2>
         * <p>Response: List of SDS Payload type list (String [][])</p>
         * <p> The list of SDS payload type list is retrieved as a result of a message history retrieving request. As each SDS Message includes a SDS payload type list, multiple SDS messages contain a list of SDS payload type list. </p>
         * <p>This is use in response to method {@link IMCOPsdk#getAllSDS(String, String, Map)}  if conversationID and recipientID parameters are not null</p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsMessageListHistory {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsMessageListHistory}</p>
         */
        public static final String SDS_PAYLOAD_LIST_OF_LIST_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".SDS_PAYLOAD_LIST_OF_LIST_TYPE";

        /**
         * <h2>Key Access to the Conversation ID </h2>
         * <p>Response: String containing an UUID conversations ID </p>
         * <p> The conversation ID is used to unequivocally identify MCdata conversation. The user can have different conversation id with the same recipient id </p>
         * <p> Used in response to method {@link IMCOPsdk#getAllSDS(String, String, Map)}  if conversationID and recipientID parameters are null</p>
         * <p> The conversation id is retrieved as a result of an incoming SDS message. </p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsSent {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsSent}</br> </p>
         * <p>sdsReceived {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsReceived}</br> </p>
         * <p>sdsError {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsError}</br> </p>
         * <p>sdsAccepted {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsAccepted}</br> </p>
         * <p>sdsNotificationSent {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsNotificationSent}</br> </p>
         * <p>sdsNotificationAccepted {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsNotificationAccepted}</br> </p>
         * <p>sdsNotificationError {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsNotificationError}</br> </p>
         * <p>sdsDeliveredNotificationReceived {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsDeliveredNotificationReceived}</br> </p>
         * <p>sdsReadNotificationReceived {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsReadNotificationReceived}</p>
         * <p>sdsMessageListHistory {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsMessageListHistory}</p>
         */
        public static final String CONVERSATION_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".CONVERSATION_ID";

        /**
         * <h2>Key Access to the Conversation ID list</h2>
         * <p>Response: Conversations ID list (String [])</p>
         * <p> The conversation ID is used to unequivocally identify MCdata conversation. The user can have different conversation id with the same recipient id </p>
         * <p> Used in response to method {@link IMCOPsdk#getAllSDS(String, String, Map)}  if conversationID parameter is null</p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsConversationListHistory {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsConversationListHistory}</p>
         */
        public static final String CONVERSATION_ID_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".CONVERSATION_ID_LIST";

        /**
         * <h2>Key Access to the timestamp, which is the UTC time in seconds since midnight UTC of January 1, 1970 (by message origin)</h2>
         * <p>Response: Calendar's time in milliseconds (long) {@link java.util.Calendar#getTimeInMillis()} </p>
         * <p> The data and time timestamp is retrieved as a result of an incoming mcdata message. </p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsReceived {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsReceived}</br> </p>
         * <p>sdsDeliveredNotificationReceived {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsDeliveredNotificationReceived}</br> </p>
         * <p>sdsReadNotificationReceived {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsReadNotificationReceived}</p>
         */
        public static final String DATE_TIME = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".DATE_TIME";

        /**
         * <h2>Key Access to the timestamp list (timestamps are in the UTC time in seconds since midnight UTC of January 1, 1970 (by message origin))</h2>
         * <p>Response:  Calendar's time in milliseconds list (long []) {@link java.util.Calendar#getTimeInMillis()} </p>
         * <p> Used in response to method {@link IMCOPsdk#getAllSDS(String, String, Map)}  if conversationID and recipientID parameters are null</p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsMessageListHistory {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsMessageListHistory}</p>
         */
        public static final String DATE_TIME_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".DATE_TIME_LIST";

        /**
         * <h2>Key Access to the ID of the message which is being replied. </h2>
         * <p>Response: String containing the UUID of the message which is being replied </p>
         * <p> The in reply to id is retrieved as a result of an incoming mcdata message. </p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsReceived {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsReceived}</p>
         */
        public static final String IN_REPLY_TO = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".IN_REPLY_TO";

        /**
         * <h2>Key Access to the list of IDs (of the message which is being replied.) </h2>
         * <p>Response: list of String containing the UUID of the message which is being replied (String []) </p>
         * <p> Used in response to method {@link IMCOPsdk#getAllSDS(String, String, Map)}  if conversationID and recipientID parameters are null</p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsMessageListHistory {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsMessageListHistory}</p>
         */
        public static final String IN_REPLY_TO_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".IN_REPLY_TO_LIST";

        /**
         * <h2>Key Access to the Message ID </h2>
         * <p>Response: String containing an UUID message ID </p>
         * <p> The message ID is used to unequivocally identify a message in a conversation. </p>
         * <p> The message id is retrieved as a result of an incoming SDS message. </p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsSent {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsSent}</br> </p>
         * <p>sdsReceived {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsReceived}</br> </p>
         * <p>sdsError {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsError}</br> </p>
         * <p>sdsAccepted {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsAccepted}</br> </p>
         * <p>sdsNotificationSent {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsNotificationSent}</br> </p>
         * <p>sdsNotificationAccepted {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsNotificationAccepted}</br> </p>
         * <p>sdsNotificationError {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsNotificationError}</br> </p>
         * <p>sdsDeliveredNotificationReceived {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsDeliveredNotificationReceived}</br> </p>
         * <p>sdsReadNotificationReceived {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsReadNotificationReceived}</p>
         */
        public static final String MESSAGE_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".MESSAGE_ID";

        /**
         * <h2>Key Access to the Message ID list </h2>
         * <p>Response: list of String containing an UUID message ID </p>
         * <p> The message ID is used to unequivocally identify a message in a conversation. </p>
         * <p> Used in response to method {@link IMCOPsdk#getAllSDS(String, String, Map)}  if conversationID and recipientID parameters are null</p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsMessageListHistory {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsMessageListHistory}</p>
         */
        public static final String MESSAGE_ID_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".MESSAGE_ID_LIST";

        /**
         * <h2>Key Access to the MCData ID </h2>
         * <p>Response: String containing a MCData ID (String)</p>
         * <p> The mcdata id is retrieved as a result of an incoming SDS message. </p>
         * <p>This is use in response from method {@link IMCOPsdk#getAllSDS(String, String, Map)}  if parameter conversationID is null  </p>
         * <p>This is use in response from method {@link IMCOPsdk#getAllSDS(String, String, Map)}  if parameters conversationID and mcdataID are not null  </p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsSent {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsSent}</br> </p>
         * <p>sdsReceived {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsReceived}</br> </p>
         * <p>sdsError {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsError}</br> </p>
         * <p>sdsAccepted {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsAccepted}</br> </p>
         * <p>sdsNotificationSent {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsNotificationSent}</br> </p>
         * <p>sdsNotificationAccepted {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsNotificationAccepted}</br> </p>
         * <p>sdsNotificationError {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsNotificationError}</br> </p>
         * <p>sdsDeliveredNotificationReceived {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsDeliveredNotificationReceived}</br> </p>
         * <p>sdsReadNotificationReceived {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsReadNotificationReceived}</br> </p>
         * <p>sdsConversationListHistory {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsConversationListHistory}</br> </p>
         * <p>sdsMessageListHistory {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsMessageListHistory}</p>
         */
        public static final String MCDATA_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".MCDATA_ID";

        /**
         * <h2>Key Access to the MCData ID list</h2>
         * <p>Response: MCData ID list (String [])</p>
         * <p> The MCData ID list is retrieved as a result of a message history retrieving request. All the user id whose conversation are available are included in the list </p>
         * <p> Used in response to method {@link IMCOPsdk#getAllSDS(String, String, Map)}  if conversationID and recipientID parameters are null</p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsRecipientIdListHistory {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsRecipientIdListHistory}</p>
         */
        public static final String MCDATA_ID_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".MCDATA_ID_LIST";

        /**
         * <h2>Key Access to the mcdata conversation type </h2>
         * <p>Response: integer representing the conversation type See {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.McDataConversationTypeEnum}  for all possible values</p>
         * <p> The Conversation type identifies if a mcdata conversation is a private or group conversation </p>
         * <p> The conversation type is retrieved as a result of an incoming SDS message. </p>
         * <p> Used in response to method {@link IMCOPsdk#getAllSDS(String, String, Map)}  if conversationID and recipientID parameters are not null</p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsReceived {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsReceived}</br> </p>
         * <p>sdsMessageListHistory {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsMessageListHistory}</p>
         */
        public static final String CONVERSATION_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".CONVERSATION_TYPE";

        /**
         * <h2>Key Access to the mcdata conversation type list </h2>
         * <p>Response: integer list representing the conversation type associated to each message. See {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.McDataConversationTypeEnum}  for all possible values</p>
         * <p> The Conversation type identifies if a mcdata conversation is a private or group conversation </p>
         * <p> Used in response to method {@link IMCOPsdk#getAllSDS(String, String, Map)} if recipientID is specified</p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsConversationListHistory {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsConversationListHistory}</p>
         */
        public static final String CONVERSATION_TYPE_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".CONVERSATION_TYPE_LIST";

        /**
         * <h2>Key Access to the MCData sender identification</h2>
         * <p>Response: String containing a MCData sender ID (String)</p>
         * <p> The MCData sender identification is used to identify the sender of the messages in group communications </p>
         * <p> The sender id is retrieved as a result of an incoming SDS message. </p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsSent {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsSent}</p>
         */
        public static final String SENDER_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".SENDER_ID";

        /**
         * <h2>Key Access to the MCData sender identification list </h2>
         * <p> Response: List of string containing MCData sender ID (String []) </p>
         * <p> The MCData sender identification is used to identify the sender of the messages in group communications </p>
         * <p> Used in response to method {@link IMCOPsdk#getAllSDS(String, String, Map)} if conversationID and recipientID parameters are not null</p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsMessageListHistory {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsMessageListHistory}</p>
         */
        public static final String SENDER_ID_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".SENDER_ID_LIST";

        /**
         * <h2>Key Access to the message notification status list </h2>
         * <p> Response: List of integers containing the message notification status (int []) </p>
         * <p> The message notification status is used to identify the notification status of the stored messages </p>
         * <p> Used in response to method {@link IMCOPsdk#getAllSDS(String, String, Map)} if conversationID and recipientID parameters are not null</p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsMessageListHistory {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsMessageListHistory}</p>
         */
        public static final String MESSAGE_NOTIFICATION_STATUS_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".MESSAGE_NOTIFICATION_STATUS_LIST";

        /**
         * <h2>Key Access to the error codes associated to the mcdata events.</h2>
         * <p>Response: Integer indicating the error code.</p>
         * <p>The mcdata event types in which this tag can be present are: </br> </p>
         * <p>sdsError {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsError}</br> </p>
         * <p>sdsNotificationError {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsNotificationError}</br> </p>
         * <p>sdsHistoryError {@link org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras.EventTypeEnum#sdsHistoryError}</p>
         * <p>Values:</p>
         *
         * <p>0 means no errors.</p>
         */
        public static final String ERROR_CODE = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".ERROR_CODE";

        /**
         * <h2>Key Access to error string associated to the mcdata events.</h2>
         * <p>Response: String describing the error.</p>
         */
        public static final String ERROR_STRING = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".ERROR_STRING";

        /**
         * <h2> Key Access to the optional tag that allow establishing data filtering criteria when request stored SDS messages.</h2>
         * <p> Response:  String representing filter start data  </p>
         * <p> It can be optionally used in sds look up operation request as a tag {@link IMCOPsdk#getAllSDS(String, String, Map)} </p>
         */
        public static final String TAG_SDS_FILTER_START_DATA = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".TAG_SDS_FILTER_START_DATA";

        /**
         * <h2> Key Access to the optional tag that allow establishing data filtering criteria when request stored SDS messages.</h2>
         * <p> Response:  String representing filter end data  </p>
         * <p> It can be optionally used in sds look up operation request as a tag {@link IMCOPsdk#getAllSDS(String, String, Map)} </p>
         */
        public static final String TAG_SDS_FILTER_END_DATA = "org.mcopenplatform.muoapi.ConstantsMCOP.MCDataEventExtras" + ".TAG_SDS_FILTER_END_DATA";

        /**
         * @enum EventTypeEnum
         * @brief Enumeration of all possible mcdata events
         *
         * <h2>Defines the different events that can occur related to MCData.</h2>
         */
        public enum EventTypeEnum {
            none(0x00),  /*!< Invalid SDS event */
            sdsSent(0x01), /*!< SDS message sent to server */
            sdsError(0x02), /*!< An already sent SDS has not been delivered */
            sdsAccepted(0x03),  /*!< An already sent SDS has been accepted */
            sdsReceived(0x04), /*!< Incoming SDS message */
            sdsNotificationSent(0x05), /*!< Notification message sent */
            sdsNotificationError(0x06), /*!< An already sent Notification has not been delivered or accepted */
            sdsNotificationAccepted(0x07),  /*!< An already sent Notification has been accepted */
            sdsDeliveredNotificationReceived (0x08), /*!< Incoming delivered Notification */
            sdsReadNotificationReceived(0x09), /*!< Incoming read Notification */
            sdsRecipientIdListHistory(0x10), /*!< MCData ID history reception event */
            sdsConversationListHistory(0x11), /*!< Conversation history reception event */
            sdsMessageListHistory(0x12), /*!< Message history reception event */
            sdsHistoryError(0x13); /*!< SDS history error event */

            private int code;

            EventTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (EventTypeEnum pageType : EventTypeEnum.values()) {
                    map.put(pageType.code, pageType);
                }
            }

            public static EventTypeEnum fromInt(int pageType) {
                return (EventTypeEnum) map.get(pageType);
            }
        }

        /**
         * @enum McDataConversationTypeEnum
         * @brief Enumeration of all possible MCData conversation types
         *
         * <h2>Defines the different MCData conversation types supported by the SDK</h2>
         */
        public enum McDataConversationTypeEnum {
            conversationTypeNone(0x00), /*!< Invalid MCData conversation type */
            conversationTypePrivate(0x01), /*!< one to one MCData conversation type */
            conversationTypeGroup(0x02);  /*!< group MCData conversation type */
            private int code;

            McDataConversationTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (McDataConversationTypeEnum pageType : McDataConversationTypeEnum.values()) {
                    map.put(pageType.code, pageType);
                }
            }

            public static McDataConversationTypeEnum fromInt(int pageType) {
                return (McDataConversationTypeEnum) map.get(pageType);
            }
        }

        /**
         * @enum SdsNotificationTypeEnum
         * @brief Enumeration of all possible SDS notification types
         *
         * <h2>Defines the different notification types that could be sent as a response to an incoming SDS message.</h2>
         */
        public enum SdsNotificationTypeEnum {
            sdsNotificationTypeNone(0x00),  /*!< Invalid SDS notification type */
            sdsNotificationTypeRead(0x01), /*!< Read SDS notification type. Indicates that the user has read the sds message */
            sdsNotificationTypeDelivered(0x02), /*!< Delivered SDS notification type. Indicates that the sds message has been delivered to the user*/
            sdsNotificationTypeDeliveredAndRead(0x03), /*!< Delivered and read SDS notification type. Indicates that the sds message has been delivered and read by the user */
            sdsNotificationTypeAccepted(0x04); /*!< Accepted SDS notification type. Indicates that the sds message has been delivered to the server */
            private int code;

            SdsNotificationTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (SdsNotificationTypeEnum pageType : SdsNotificationTypeEnum.values()) {
                    map.put(pageType.code, pageType);
                }
            }

            public static SdsNotificationTypeEnum fromInt(int pageType) {
                return (SdsNotificationTypeEnum) map.get(pageType);
            }
        }

        /**
         * @enum SdsPayloadTypeEnum
         * @brief Enumeration of all possible SDS payload types
         *
         * <h2>Defines the different payload types that could be sent within a SDS message.</h2>
         */
        public enum SdsPayloadTypeEnum {
            sdsPayloadTypeNone(0x00),
            sdsPayloadTypeText(0x01),
            sdsPayloadTypeHyperlink(0x02),
            sdsPayloadTypeBinary(0x03),
            sdsPayloadTypeLocation(0x04);

            private int code;

            SdsPayloadTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (SdsPayloadTypeEnum pageType : SdsPayloadTypeEnum.values()) {
                    map.put(pageType.code, pageType);
                }
            }

            public static SdsPayloadTypeEnum fromInt(int pageType) {
                return (SdsPayloadTypeEnum) map.get(pageType);
            }
        }

    }

    /**
     * @brief Media stream control mechanisms related event. Manages the events regarding to floor control and transmission control
     *
     * <h1>Key Access to the values of {@link ConstantsMCOP.ActionsCallBack#controlMechanismEvent} event types </h1>
     * <p>This class contains all the answers to the actions of methods {@link IMCOPsdk#controlMechanismOperation(String, int, Map)} </p>
     */
    public static class ControlMechanismEventExtras {

        /**
         * <h2> Key Access to the control mechanism event type. See {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum} for all possible values.</h2>
         * <p>Response: Integer type value </p>
         * <p>This parameter is included in all call type events {@link ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum}</p>
         */
        public static final String CONTROL_MECHANISM_EVENT = "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras" + ".CONTROL_MECHANISM_EVENT";

        /**
         * <h2>Key Access to the human-readable display name of the user who is talking at each moment </h2>
         * <p>Response: String that identifies the user with a human-readable alias</p>
         * <p>The control mechanism event types in which this tag can be present are: </br> </p>
         * <p>taken {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#taken}</p>
         */
        public static final String DISPLAY_NAME = "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras" + ".DISPLAY_NAME";

        /**
         * <h2>Key Access to the number of seconds that the client can speak when token has been granted</h2>
         * <p>Response: Integer value representing the seconds that the client can speak  </p>
         * <p>The control mechanism event types in which this tag can be present are: </br> </p>
         * <p>granted {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#granted}</p>
         */
        public static final String TOKEN_DURATION = "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras" + ".TOKEN_DURATION";

        /**
         * <h2>Key Access to the time warning configured number of seconds.</h2>
         * <p> When the client is talking this represents the number of token duration remaining seconds when the user will be alerted </p>
         * <p>Response: Integer value representing time warning configured number of seconds.</p>
         * <p>The control mechanism event types in which this tag can be present are: </br> </p>
         * <p>granted {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#granted}</p>
         */
        public static final String TOKEN_DURATIONG_WARNING_TIME = "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras" + ".TOKEN_DURATIONG_WARNING_TIME";

        /**
         * <h2>Key Access to the maximum inactivity number of seconds. This indicates the time after which the call will be terminated due to inactivity</h2>
         * <p>Response: Integer value representing the maximum inactivity number of seconds</p>
         * <p>The control mechanism event types in which this tag can be present are: </br> </p>
         * <p>idle {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#idle}</p>
         */
        public static final String INACTIVITY_HANG_TIME = "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras" + ".INACTIVITY_HANG_TIME";

        /**
         * <h2>Key Access to the position of the performed request, inside the queue. </h2>
         * <p>Response: Integer representing the queue position. It can take a value between 0-255. 254 means that the client is not queued. </p>
         * <p>The control mechanism event types in which this tag can be present are: </br> </p>
         * <p>queued {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#queued}</p>
         */
        public static final String QUEUED_POSITION="org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras"+".QUEUED_POSITION";

        /**
         * <h2>Key Access to the priority of the floor request in the queue </h2>
         * <p>Response: Integer representing the queue priority. It can take a value between 0-255.</p>
         * <p>The control mechanism event types in which this tag can be present are: </br> </p>
         * <p>queued {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#queued}</p>
         */
        public static final String QUEUED_PRIORITY="org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras"+".QUEUED_PRIORITY";

        /**
         * <h2>Key Access to the MCPTT/MCVIDEO ID of the user causing the event</h2>
         * <p>Response: String type value representing the identifier </p>
         * <p>The control mechanism event types in which this tag can be present are: </br> </p>
         * <p>taken {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#taken}</br> </p>
         * <p>reception_granted {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#reception_granted}</br> </p>
         * <p>transmission_end_response {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#transmission_end_response}</br> </p>
         * <p>reception_end_response {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#reception_end_response}</p>
         */
        public static final String USER_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras" + ".USER_ID";

        /**
         * <h2>Key Access to the functional alias of the user causing the event</h2>
         * <p>Response: String type value representing the identifier </p>
         * <p>The control mechanism event types in which this tag can be present are: </br> </p>
         * <p>taken {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#taken}</br> </p>
         * <p>reception_granted {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#reception_granted}</br> </p>
         * <p>transmission_end_response {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#transmission_end_response}</br> </p>
         * <p>reception_end_response {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#reception_end_response}</p>
         */
        public static final String FUNCTIONAL_ALIAS_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras" + ".FUNCTIONAL_ALIAS_ID";

        /**
         * <h2>Key Access to geographic location longitude data of the user causing the event </h2>
         * <p>Response: String type value representing the identifier </p>
         * <p>The control mechanism event types in which this tag can be present are: </br> </p>
         * <p>taken {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#taken}</br> </p>
         * <p>reception_granted {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#reception_granted}</br> </p>
         * <p>transmission_end_response {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#transmission_end_response}</br> </p>
         * <p>reception_end_response {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#reception_end_response}</p>
         */
        public static final String USER_GEO_LONGITUDE = "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras" + ".USER_GEO_LONGITUDE";

        /**
         * <h2>Key Access to geographic location latitude data of the user causing the event </h2>
         * <p>Response: String type value representing the identifier </p>
         * <p>The control mechanism event types in which this tag can be present are: </br> </p>
         * <p>taken {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#taken}</br> </p>
         * <p>reception_granted {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#reception_granted}</br> </p>
         * <p>transmission_end_response {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#transmission_end_response}</br> </p>
         * <p>reception_end_response {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#reception_end_response}</p>
         */
        public static final String USER_GEO_LATITUDE = "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras" + ".USER_GEO_LATITUDE";

        /**
         * <h2>Key Access to the indication representing if the permission to transmit can be requested despite being in #taken state </h2>
         * <p>Response: boolean value. True means that requests could be performed. </p>
         * <p>The control mechanism event types in which this tag can be present are: </br> </p>
         * <p>taken {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#taken}</p>
         */
        public static final String REQUEST_ALLOWED = "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras" + ".REQUEST_ALLOWED";

        /**
         * <h2>Key Access to the call session identifier. This identifier is unique for each call.</h2>
         * <p>Response: String type value </p>
         * <p>This parameter is included in all control mechanism type events {@link ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum}</p>
         */
        public static final String SESSION_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras" + ".SESSION_ID";

        /**
         * <h2>Key Access to the call type. See {@link org.mcopenplatform.muoapi.ConstantsMCOP.CallEventExtras.CallTypeEnum} for all possible values.</h2>
         * <p>Response: Integer type value </p>
         * <p>This parameter is included in all control mechanism type events {@link ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum}</p>
         */
        public static final String CALL_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras" + ".CALL_TYPE";

        /**
         * <h2>Key Access to the cause code of the DENY or REVOKE events received as a response to a media control mechanism related request</h2>
         * <p>Response: Integer indicating the code of the cause.</p>
         * <p>Values:</p>
         * <p>  Event  {@link ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#denied}:</p>
         * <p> 101     Cause #1 - Another MCPTT client has permission The <Reject cause> value set to '1' indicates that another MCPTT user has permission to send a media.</p>
         * <p> 102     Cause #2 - Internal floor control server error The <Reject cause> value set to '2' indicates that the floor control server cannot grant the floor request due to an internal error.</p>
         * <p> 103     Cause #3 - Only one participant The <Reject cause> value set to '3' indicates that the floor control server cannot grant the floor request, because the requesting party is the only participant in the MCPTT session.</p>
         * <p> 104     Cause #4 - Retry-after timer has not expired The <Reject cause> value set to '4' indicates that the floor control server cannot grant the floor request, because timer T9 (Retry-after) has not expired after permission to send media has been revoked.</p>
         * <p> 105     Cause #5 - Receive only The <Reject cause> value set to '5' indicates that the floor control server cannot grant the floor request, because the requesting party only has receive privilege.</p>
         * <p> 106     Cause #6 - No resources available The <Reject cause> value set to '6' indicates that the floor control server cannot grant the floor request due to congestion.</p>
         * <p> 107     Cause #7 – Queue full The <Reject cause> value set to 7 indicates that the floor control server cannot queue the floor request, because the queue is full.</p>
         * <p> 108     Cause #255 - Other reason The <Reject cause> value set to '255' indicates that the floor control server does not grant the floor request due to the floor control server local policy.</p>
         * <p>  Event  {@link ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#revoked}:</p>
         * <p> 201     Cause #1 – Only one MCPTT client The <Reject Cause> value set to '1' indicates that the MCPTT client is the only MCPTT client in the MCPTT session or the only participant connected to a floor control server. No additional information included.</p>
         * <p> 202     Cause #2 – Media burst too long The <Reject Cause> value set to '2' indicates that the MCPTT User has talked too long (e.g., the stop-talking timer has expired). No additional information included.</p>
         * <p> 203     Cause #3 - No permission to send a Media Burst The <Reject Cause> value set to '3' indicates that the MCPTT client does not have permission to send media. No additional information is included.</p>
         * <p> 204     Cause #4 - Media Burst pre-empted The <Reject Cause> value set to '4' indicates that the MCPTT client 's permission to send a media is being pre-empted. No additional information is included.</p>
         * <p> 205     Cause #6 - No resources available The <Reject Cause> value set to '6' indicates that the floor control server can no longer grant MCPTT client to send media due to congestion. No additional information is included.</p>
         * <p> 206     Cause #255 – Other reason The <Reject Cause> value set to '255' indicates that the floor control server can no longer grant MCPTT client to send media due to the floor control server local policy. No additional information is included.</p>
         * <p>  Event  {@link ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#transmission_rejection}:</p>
         * <p> 301     Cause #1 - Transmission limit reached The <Reject cause> value set to '1' indicates that the number of transmitters have reached maximum.</p>
         * <p> 302     Cause #2 - Internal transmission control server error The <Reject cause> value set to '2' indicates that the transmission control server cannot grant the transmission request due to an internal error.</p>
         * <p> 303     Cause #3 - Only one participant The <Reject cause> value set to '3' indicates that the transmission control server cannot grant the transmission request, because the requesting party is the only participant in the MCVideo session.</p>
         * <p> 304     Cause #4 - Retry-after timer has not expired The <Reject cause> value set to '4' indicates that the transmission control server cannot grant the transmission request, because timer T9 (Retry-after) has not expired after permission to send media has been revoked.</p>
         * <p> 305     Cause #5 - Receive only The <Reject cause> value set to '5' indicates that the transmission control server cannot grant the transmission request, because the requesting party only has receive privilege.</p>
         * <p> 306     Cause #6 - No resources available The <Reject cause> value set to '6' indicates that the transmission control server cannot grant the transmission request due to congestion.</p>
         * <p> 307     Cause #255 - Other reason The <Reject cause> value set to '255' indicates that the transmission control server does not grant the transmission request due to the transmission control server local policy.</p>
         * <p>  Event  {@link ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#transmission_revoke}:</p>
         * <p> 401     Cause #1 – Only one MCVideo client The <Reject Cause> value set to '1' indicates that the MCVideo client is the only MCVideo client in the MCVideo session or the only participant connected to a transmission control server. No additional information included.</p>
         * <p> 402     Cause #2 – Media burst too long The <Reject Cause> value set to '2' indicates that the MCVideo User has transmitted too long (e.g., the stop-transmission timer has expired). No additional information included.</p>
         * <p> 403     Cause #3 - No permission to send a Media Burst The <Reject Cause> value set to '3' indicates that the MCVideo client does not have permission to send media. No additional information is included.</p>
         * <p> 404     Cause #4 - Media Burst pre-empted The <Reject Cause> value set to '4' indicates that the MCVideo client's permission to send a media is being pre-empted. No additional information is included.</p>
         * <p> 405     Cause #6 - No resources available The <Reject Cause> value set to '6' indicates that the transmission control server can no longer grant MCVideo client to send media due to congestion. No additional information is included.</p>
         * <p> 406     Cause #255 – Other reason The <Reject Cause> value set to '255' indicates that the transmission control server can no longer grant MCVideo client to send media due to the transmission control server local policy. No additional information is included.</p>
         * <p>  Event  {@link ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#reception_rejection}:</p>
         * <p> 501     Cause #2 - Internal transmission control server error The <Reject cause> value set to '2' indicates that the transmission control server cannot grant the receive media request due to an internal error.</p>
         * <p> 502     Cause #4 - Retry-after timer has not expired The <Reject cause> value set to '4' indicates that the transmission control server cannot grant the receive media request, because timer T9 (Retry-after) has not expired after permission to send media has been revoked.</p>
         * <p> 503     Cause #5 - Send only The <Reject cause> value set to '5' indicates that the transmission control server cannot grant the receive media request, because the requesting party only has send privilege.</p>
         * <p> 504     Cause #6 - No resources available The <Reject cause> value set to '6' indicates that the transmission control server cannot grant the receive media request due to congestion.</p>
         * <p> 505     Cause #255 - Other reason The <Reject cause> value set to '255' indicates that the transmission control server does not grant the receive media request due to the transmission control server local policy.</p>
         * <p>The control mechanism event types in which this tag can be present are: </br> </p>
         * <p>taken {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#taken}</p>
         */
        public static final String CAUSE_CODE = "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras" + ".CAUSE_CODE";

        /**
         * <h2>Key Access to the cause string of the DENY or REVOKE events received as a response to a media control mechanism related request.</h2>
         * <p>Response: String describing the cause </p>
         * <p>The control mechanism event types in which this tag can be present are: </br> </p>
         * <p>denied {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#denied}</br> </p>
         * <p>revoked {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#revoked}</p>
         */
        public static final String CAUSE_STRING = "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras" + ".CAUSE_STRING";

        /**
         /**
         * <h2>Key Access to the error codes associated to the control mechanism events.</h2>
         * <p>Response: Integer indicating the error code.</p>
         * <p>Values:</p>
         * <p>Code	Explanatory text	Description</p>
         * <p>101	The session cannot be accepted because it has not received any session for that sessionID</p>
         * <p>102	Floor control invalid operation</p>
         * <p>103	maximum simultaneous MCPTT group calls reached	The number of maximum simultaneous MCPTT group calls supported for the MCPTT user has been exceeded.</p>
         * <p>104	isfocus not assigned	A controlling MCPTT function has not been assigned to the MCPTT session.</p>
         * <p>The control mechanism event types in which this tag can be present are: </br> </p>
         * <p>error {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#error}</p>
         */
        public static final String ERROR_CODE = "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras" + ".ERROR_CODE";

        /**
         * <h2>Key Access to error string associated to the control mechanism events</h2>
         * <p>Response: String describing the error.</p>
         * <p>The control mechanism event types in which this tag can be present are: </br> </p>
         * <p>error {@link org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.ControlMechanismEventTypeEnum#error}</p>
         */
        public static final String ERROR_STRING = "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras" + ".ERROR_STRING";

        /**
         * <h2> Key Access to the control mechanism optional priority setting tag.</h2>
         * <p> Used to set the priority level of a request. It can take a value between 0-255.<p>
         * <p> It can be used in control mechanism requests to set the priority level of a request {@link IMCOPsdk#controlMechanismOperation(String, int, Map)}  </p>
         * <p> Contains a integer between 0-255.  </p>
         */
        public static final String TAG_CTRL_MECH_PRIO = "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras" + ".TAG_CTRL_MECH_PRIO";

        /**
         * <h2> Key Access to the MCVideo user identification tag.</h2>
         * <p> Used in transmission control mechanism in MCVideo session to select the user referred in the request.<p>
         * <p> It can be used in transmission control mechanism to set user whose video media wants to be received {@link IMCOPsdk#controlMechanismOperation(String, int, Map)}  </p>
         * <p> Contains a string type value representing the identifier.  </p>
         */
        public static final String TAG_CTRL_MECH_MCVIDEO_USER_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras" + ".TAG_CTRL_MECH_MCVIDEO_USER_ID";

        /**
         * @enum ControlMechanismOperationTypeEnum
         * @brief Floor request types
         *
         * <h2>Definition of all possible floor/transmission control request types</h2>
         *
         */
        public enum ControlMechanismOperationTypeEnum {
            ControlMechanismNone(0x00), /*!< Invalid request type */
            ControlMechanismFloorRequest(0x01), /*!< Floor request. Request permission to talk */
            ControlMechanismFloorRelease(0x02),  /*!< Floor release. This event can be used to release the token, when granted, or even cancel a queued floor request. */
            ControlMechanismQueuePositionInfoRequest(0x03), /*!< Floor queue position request. This request can be use to request the queue position of the user */
            ControlMechanismTransmissionRequest(0x04), /*!< Transmission Request message. This is a request from a transmission participant to get permission to send media */
            ControlMechanismTransmissionEndRequest(0x05), /*!< Transmission Release message. This is a request from the transmission participant to the transmission control server to inform that the transmission can be released */
            ControlMechanismReceptionRequest(0x06),
            ControlMechanismReceptionEndRequest(0x07);
            private int code;

            ControlMechanismOperationTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (ControlMechanismOperationTypeEnum pageType : ControlMechanismOperationTypeEnum.values()) {
                    map.put(pageType.code, pageType);
                }
            }

            public static ControlMechanismOperationTypeEnum fromInt(int pageType) {
                return (ControlMechanismOperationTypeEnum) map.get(pageType);
            }
        }

        /**
         * @enum ControlMechanismEventTypeEnum
         * @brief Enumeration of all possible media control mechanisms related event types
         *
         * <h2>Definition of all possible floor/transmission control event types. Received on key {@link ConstantsMCOP.ControlMechanismEventExtras#CONTROL_MECHANISM_EVENT} events.</h2>
         */
        public enum ControlMechanismEventTypeEnum {
            none(""),  /*!< Invalid control mechanism event */
            error("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".error"), /*!< Internal error report */
            granted("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".granted"), /*!< Floor granted. Grants permission to talk */
            taken("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".taken"), /*!< Floor taken. Someone else has permission to talk */
            denied("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".denied"),  /*!< Floor denied. The server denies permission to talk */
            revoked("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".revoked"),  /*!< Floor revoked. The server revokes the permission to talk due to some specific reason (e.g exceeded assigned time slot to talk)*/
            queued("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".queued"), /*!< Floor queued. The user is not granted permission yet, but the request is queued, based on priority */
            queued_timeout("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".queued_timeout"), /*!< Floor queued timeout. The request has been queued for too long */
            queue_released("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT"+".queue_released"), /*!< Floor queued released . The request has been removed from the queued */
            idle("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".idle"), /*!< Floor idle. No one is talking nor has permission to talk */
            request_sent("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".request_sent"),  //TODO: Complete description
            release_sent("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".release_sent"),  //TODO: Complete description
            transmission_granted("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".transmission_granted"),  //TODO: Complete description
            reception_granted("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".reception_granted"),  //TODO: Complete description
            transmission_rejection("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".transmission_rejection"),  //TODO: Complete description
            reception_rejection("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".reception_rejection"),  //TODO: Complete description
            transmission_revoke("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".transmission_revoke"),  //TODO: Complete description
            reception_revoke("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".reception_revoke"),  //TODO: Complete description
            transmission_notification("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".transmission_notification"),  //TODO: Complete description
            transmission_end_notification(
                    "org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".transmission_end_notification"),  //TODO: Complete description
            transmission_end_response("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".transmission_end_response"),  //TODO: Complete description
            reception_end_response("org.mcopenplatform.muoapi.ConstantsMCOP.ControlMechanismEventExtras.CONTROL_MECHANISM_EVENT" + ".reception_end_response");  //TODO: Complete description

            private final String text;

            /**
             * @param text
             */
            ControlMechanismEventTypeEnum(final String text) {
                this.text = text;
            }

            /* (non-Javadoc)
             * @see java.lang.Enum#toString()
             */
            @Override
            public String toString() {
                return text;
            }

            public static ControlMechanismEventTypeEnum fromString(String setText) {
                for (ControlMechanismEventTypeEnum value : ControlMechanismEventTypeEnum.values()) {
                    if (value.text.equalsIgnoreCase(setText)) {
                        return value;
                    }
                }
                return null;
            }
        }
    }

    /**
     * @brief Emergency alert related event. Manages the events regarding emergency alert
     *
     * <h1>Key Access to the values of {@link ConstantsMCOP.ActionsCallBack#emergencyAlertEvent} event types.</h1>
     * <p>This class contains all the answers to the actions of method {@link IMCOPsdk#setGroupEmergencyAlertState(String, boolean, Map)} </p>
     */
    public static class EmergencyAlertEventExtras{

        /**
         * <h2>Key Access to the emergency alert event type. See {@link org.mcopenplatform.muoapi.ConstantsMCOP.EmergencyAlertEventExtras.EmergencyAlertEventTypeEnum} for all possible values.</h2>
         * <p>Response: Integer type value </p>
         * <p>This parameter is included in all call type events {@link ConstantsMCOP.EmergencyAlertEventExtras.EmergencyAlertEventTypeEnum}</p>
         */
        public static final String EVENT_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.EmergencyAlertEventExtras" + ".EVENT_TYPE";

        /**
         * <h2>Key Access to the emergency alert type. Determines if the emergency alert is related to a private or group call </h2>
         * <p>Response: Integer type value. See {@link org.mcopenplatform.muoapi.ConstantsMCOP.EmergencyAlertEventExtras.EmergencyAlertType} for all possible values. </p>
         * <p>This parameter is included in all call type events {@link ConstantsMCOP.EmergencyAlertEventExtras.EmergencyAlertEventTypeEnum}</p>
         */
        public static final String EMERGENCY_ALERT_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.EmergencyAlertEventExtras" + ".EMERGENCY_ALERT_TYPE";

        /**
         * <h2>Key Access to the group for which the status change operation is being performed </h2>
         * <p>Response: String type value representing the identifier </p>
         * <p>This parameter is included in all emergency alert type events (in case of group emergency alert type ) {@link ConstantsMCOP.EmergencyAlertEventExtras.EmergencyAlertEventTypeEnum}</p>
         */
        public static final String GROUP_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.EmergencyAlertEventExtras" + ".GROUP_ID";

        /**
         * <h2>Key Access to the user id of the originating user of an emergency alert when being cancelled by another authorised user </h2>
         * <p>Response: String type value representing the identifier </p>
         * <p>The emergency alert mechanism event types in which this tag can be present are: </br> </p>
         * <p>EmergencyAlertClear {@link org.mcopenplatform.muoapi.ConstantsMCOP.EmergencyAlertEventExtras.EmergencyAlertEventTypeEnum#EmergencyAlertClear}</br> </p>
         * <p>EmergencyAlertRemoteClear {@link org.mcopenplatform.muoapi.ConstantsMCOP.EmergencyAlertEventExtras.EmergencyAlertEventTypeEnum#EmergencyAlertRemoteClear}</p>
         */
        public static final String ORIGINATED_BY_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.EmergencyAlertEventExtras" + ".ORIGINATED_BY_ID";

        /**
         * <h2>Key Access to the MCS Identity of the user who realize the status change operation</h2>
         * <p>Response: String type value representing the identifier </p>
         * <p>This parameter is included in all emergency alert type events {@link ConstantsMCOP.EmergencyAlertEventExtras.EmergencyAlertEventTypeEnum}</p>
         */
        public static final String USER_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.EmergencyAlertEventExtras" + ".USER_ID";

        /**
         * <h2>Key Access to the user functional alias who realize the status change operation </h2>
         * <p>Response: String type value representing the functional alias </p>
         * <p>This parameter is included in all emergency alert type events {@link ConstantsMCOP.EmergencyAlertEventExtras.EmergencyAlertEventTypeEnum}</p>
         */
        public static final String USER_FUNCTIONAL_ALIAS = "org.mcopenplatform.muoapi.ConstantsMCOP.EmergencyAlertEventExtras" + ".USER_FUNCTIONAL_ALIAS";

        /**
         * <h2>Key Access to the user Mission Critical Organization </h2>
         * <p>Response: String type value representing organization identifier </p>
         * <p>This parameter is included in all emergency alert type events {@link ConstantsMCOP.EmergencyAlertEventExtras.EmergencyAlertEventTypeEnum}</p>
         */
        public static final String ORGANIZATION_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.EmergencyAlertEventExtras" + ".ORGANIZATION_ID";

        /**
         * <h2>Key Access to the error codes associated to the emergency alert events.</h2>
         * <p>Response: Integer indicating the error code.</p>
         * <p>The emergency alert event types in which this tag can be present are: </br> </p>
         * <p>EmergencyAlertOperationError {@link org.mcopenplatform.muoapi.ConstantsMCOP.EmergencyAlertEventExtras.EmergencyAlertEventTypeEnum#EmergencyAlertOperationError}</p>
         * <p>Values:</p>
         * <p>0 means no errors.</p>
         *
         */
        public static final String ERROR_CODE = "org.mcopenplatform.muoapi.ConstantsMCOP.EmergencyAlertEventExtras" + ".ERROR_CODE";

        /**
         * <h2>Key Access to error string associated to the emergency alert events.</h2>
         * <p>Response: String describing the error.</p>
         */
        public static final String ERROR_STRING = "org.mcopenplatform.muoapi.ConstantsMCOP.EmergencyAlertEventExtras" + ".ERROR_STRING";

        /**
         * <h2> Key Access to the emergency alert originator tag.</h2>
         * <p> Used in emergency alert cancellation operation request to indicate the user that has initially generated the alert that wants to be cleared.<p>
         * <p> It can be optionally used in emergency alert operations {@link IMCOPsdk#setGroupEmergencyAlertState(String, boolean, Map)} </p>
         * <p> Contains a string type value representing the identifier.  </p>
         */
        public static final String TAG_EMERGENCY_ALERT_ORIGINATED_BY = "org.mcopenplatform.muoapi.ConstantsMCOP.EmergencyAlertEventExtras" + ".TAG_EMERGENCY_ALERT_ORIGINATED_BY";

        /**
         * @enum EmergencyAlertEventTypeEnum
         * @brief Enumeration of all possible emergency alert event types
         *
         * Defines the different events that can occur related to emergency alert
         */
        public enum EmergencyAlertEventTypeEnum {

            EmergencyAlertNone(0x00), /*!< Invalid emergency alert event */
            EmergencyAlertSet(0x01), /*!< User emergency alert has been set */
            EmergencyAlertClear(0x02), /*!< User emergency alert has been clear */
            EmergencyAlertRemoteSet(0x03), /*!< Remote user emergency alert has been set */
            EmergencyAlertRemoteClear(0x04), /*!< Remote user emergency alert has been clear */
            EmergencyAlertOperationSuccess (0x05),  /*!< Emergency alert operation success */
            EmergencyAlertOperationError (0x06); /*!< Emergency alert operation error */

            private int code;

            EmergencyAlertEventTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (EmergencyAlertEventExtras.EmergencyAlertEventTypeEnum pageType : EmergencyAlertEventExtras.EmergencyAlertEventTypeEnum.values()) {
                    map.put(pageType.code, pageType);
                }
            }

            public static EmergencyAlertEventExtras.EmergencyAlertEventTypeEnum fromInt(int pageType) {
                return (EmergencyAlertEventExtras.EmergencyAlertEventTypeEnum) map.get(pageType);
            }
        }

        /**
         * @enum EmergencyAlertType
         * @brief Enumeration of all possible emergency alert types
         *
         * Defines the different emergency alert types that can occur
         */
        public enum EmergencyAlertType {

            EmergencyAlertNone(0x00), /*!< Invalid emergency alert type */
            EmergencyAlertPrivate(0x01), /*!< Emergency alert is related to a private call */
            EmergencyAlertGroup(0x02); /*!<Emergency alert is related to a group */

            private int code;

            EmergencyAlertType(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (EmergencyAlertEventExtras.EmergencyAlertType pageType : EmergencyAlertEventExtras.EmergencyAlertType.values()) {
                    map.put(pageType.code, pageType);
                }
            }

            public static EmergencyAlertEventExtras.EmergencyAlertType fromInt(int pageType) {
                return (EmergencyAlertEventExtras.EmergencyAlertType) map.get(pageType);
            }
        }
    }

    /**
     * @brief User configuration related related event. Manages the events regarding configuration changes
     *
     * <h1>Key Access to the values of {@link ConstantsMCOP.ActionsCallBack#configurationInfoEvent} event types </h1>
     * <p>This class contains all the answers to the actions of method {@link IMCOPsdk#getConfigurationInfo()} </p>
     */
    public static class ConfigurationInfoEventExtras {

        /**
         * <h2>Key Access to the configuration event type. See {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum} for all possible values.</h2>
         * <p>Response: String type value </p>
         */
        public static final String EVENT_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".EVENT_TYPE";

        /**
         * <h2>Key Access to the human-readable display name of the user</h2>
         * <p>Response: String that identifies the user with a human-readable alias, more pleasant than a MCX identifier.</p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcpttUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcpttUserConf}</br> </p>
         * <p>ConfigurationInfoMcdataUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcdataUserConf}</br> </p>
         * <p>ConfigurationInfoMcvideoUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcvideoUserConf}</p>
         */
        public static final String DISPLAY_NAME = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".DISPLAY_NAME";


        /**
         * <h2>Key Access to the MCX organization that the user belongs to.</h2>
         * <p>Response: String containing the MCX Organization</p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcpttUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcpttUserConf}</br> </p>
         * <p>ConfigurationInfoMcdataUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcdataUserConf}</br> </p>
         * <p>ConfigurationInfoMcvideoUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcvideoUserConf}</p>
         */
        public static final String ORGANIZATION = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".ORGANIZATION";

        /**
         * <h2>Key Access to ParticipantType, indicating the position of the user in the organization.</h2>
         * <p>Response: String indicating the user position.</p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcpttUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcpttUserConf}</br> </p>
         * <p>ConfigurationInfoMcdataUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcdataUserConf}</br> </p>
         * <p>ConfigurationInfoMcvideoUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcvideoUserConf}</p>
         */
        public static final String PARTICIPANT_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".PARTICIPANT_TYPE";

        /**
         * <h2>Key Access to the default contact configured for emergency MCPTT/MCVideo private calls</h2>
         * <p>Response: String with UserID format</p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcpttUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcpttUserConf}</br> </p>
         * <p>ConfigurationInfoMcvideoUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcvideoUserConf}</p>
         */
        public static final String DEFAULT_EMERGENCY_CONTACT = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".DEFAULT_EMERGENCY_CONTACT";

        /**
         * <h2>Key Access to the default group configured for emergency MCPTT/MCVideo private calls</h2>
         * <p>Response: String with group identifier format</p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcpttUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcpttUserConf}</br> </p>
         * <p>ConfigurationInfoMcvideoUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcvideoUserConf}</p>
         */
        public static final String DEFAULT_EMERGENCY_GROUP = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".DEFAULT_EMERGENCY_GROUP";

        /**
         * <h2>Key Access to the list of user granted permissions for MCPTT service.</h2>
         * <p>Response: Integer with values of all the FLAGs that identify user allowed functions</p>
         * <p> See {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.McpttAllowTypeEnum} for all possible values </p>
         * <p>NOTE: The absence of permissions in this list indicates that it is not allowed to make such type of calls, and any attempt will give an error in {@link CallEventExtras.CallEventTypeEnum#CallEventError}.</p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcpttUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcpttUserConf}</p>
         */

        public static final String MCPTT_ALLOWS_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".MCPTT_ALLOWS_LIST";

        /**
         * <h2>Key Access to the list of user granted permissions for MCDATA service.</h2>
         * <p>Response: Integer with values of all the FLAGs that identify user allowed functions</p>
         * <p> See {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.McdataAllowTypeEnum} for all possible values </p>
         * <p>NOTE: The absence of permissions in this list indicates that it is not allowed to make such type of calls, and any attempt will give an error in {@link MCDataEventExtras.EventTypeEnum#sdsError}.</p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcdataUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcdataUserConf}</p>
         */

        public static final String MCDATA_ALLOWS_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".MCDATA_ALLOWS_LIST";

        /**
         * <h2>Key Access to the list of user granted permissions for MCVIDEO service.</h2>
         * <p>Response: Integer with values of all the FLAGs that identify user allowed functions</p>
         * <p> See {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.McvideoAllowTypeEnum} for all possible values </p>
         * <p>NOTE: The absence of permissions in this list indicates that it is not allowed to make such type of calls, and any attempt will give an error in {@link CallEventExtras.CallEventTypeEnum#CallEventError}.</p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcvideoUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcvideoUserConf}</p>
         */

        public static final String MCVIDEO_ALLOWS_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".MCVIDEO_ALLOWS_LIST";

        /**
         * <h2>Access Key to the Private contacts available for the user</h2>
         * <p>Response: Array of strings with Sips URI  of the available contacts (List<String>)</p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcpttPrivateContacts {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcpttPrivateContacts}</br> </p>
         * <p>ConfigurationInfoMcdataPrivateContacts {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcdataPrivateContacts}</br> </p>
         * <p>ConfigurationInfoMcvideoPrivateContacts {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcvideoPrivateContacts}</p>
         */

        public static final String PRIVATE_CONTACT_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".PRIVATE_CONTACT_LIST";

        /**
         * <h2>Access Key to the display name of private contacts available for the user</h2>
         * <p>Response: Array of strings with the display name of the available contacts (List<String>)</p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcpttPrivateContacts {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcpttPrivateContacts}</br> </p>
         * <p>ConfigurationInfoMcdataPrivateContacts {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcdataPrivateContacts}</br> </p>
         * <p>ConfigurationInfoMcvideoPrivateContacts {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcvideoPrivateContacts}</p>

         */

        public static final String PRIVATE_CONTACT_DISPLAY_NAME_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".PRIVATE_CONTACT_DISPLAY_NAME_LIST";

        /**
         * <h2>Access Key to the transaction identifier</h2>
         * <p>This identifier will allow sending the data in multiple events to avoid datatoolarge exceptions through the AIDL</p>
         * <p>Response: Long with the identifier</p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcpttPrivateContacts {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcpttPrivateContacts}</br> </p>
         * <p>ConfigurationInfoMcdataPrivateContacts {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcdataPrivateContacts}</br> </p>
         * <p>ConfigurationInfoMcvideoPrivateContacts {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcvideoPrivateContacts}</p>
         */

        public static final String TRANSACTION_IDENTIFIER = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".TRANSACTION_IDENTIFIER";

        /**
         * <h2>Access Key to the element total amount number</h2>
         * <p>This identifier will allow sending the data in multiple events to avoid datatoolarge exceptions through the AIDL</p>
         * <p>Response: Long with the quantity</p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcpttPrivateContacts {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcpttPrivateContacts}</br> </p>
         * <p>ConfigurationInfoMcdataPrivateContacts {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcdataPrivateContacts}</br> </p>
         * <p>ConfigurationInfoMcvideoPrivateContacts {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcvideoPrivateContacts}</p>
         */

        public static final String TRANSACTION_TOTAL_ELEMENTS = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".TRANSACTION_TOTAL_ELEMENTS";

        /**
         * <h2>Access Key to the SDS predefined messages list</h2>
         * <p>Response: Map with the relation of sds predefined messages with them corresponding code (Map<String,String>) </p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcdataUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcdataUserConf}</p>
         */

        public static final String SDS_PREDEFINED_MESSAGE_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".SDS_PREDEFINED_MESSAGE_LIST";

        /**
         * <h2>Access Key to the maximum sds size </h2>
         * <p>This data will be received once logged into the system or whenever the user receives a new configuration.</p>
         * <p>Response: Long with the maximum characters that can be included in a SDS message</p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcdataServiceConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcdataServiceConf}</p>
         */

        public static final String SDS_MAXIMUM_CHARACTER_LIMIT = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".SDS_MAXIMUM_CHARACTER_LIMIT";

        /**
         * <h2>Key Access to the list of groups IDs the user if member of.</h2>
         * <p>Response: List<String> containing the identifier of the groups the user is member of</p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcpttUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcpttUserConf}</br> </p>
         * <p>ConfigurationInfoMcdataUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcdataUserConf}</br> </p>
         * <p>ConfigurationInfoMcvideoUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcvideoUserConf}</p>
         */
        public static final String GROUP_ID_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".GROUP_LIST";

        /**
         * <h2>Key Access to the list of groups display name the user if member of.</h2>
         * <p>Response: List<String> containing the display name of the groups the user is member of</p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcpttUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcpttUserConf}</br> </p>
         * <p>ConfigurationInfoMcdataUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcdataUserConf}</br> </p>
         * <p>ConfigurationInfoMcvideoUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcvideoUserConf}</p>
         */
        public static final String GROUP_DISPLAY_NAME_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".GROUP_LIST";

        /**
         * <h2>Key Access to the list of functional alias uris available for the user </h2>
         * <p>Response: List<String> that contains the functional alias identifier of every available functional alias </p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcpttUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcpttUserConf}</br> </p>
         * <p>ConfigurationInfoMcdataUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcdataUserConf}</br> </p>
         * <p>ConfigurationInfoMcvideoUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcvideoUserConf}</p>
         */
        public static final String USER_FUNCTIONAL_ALIAS_URI_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".USER_FUNCTIONAL_ALIAS_URI_LIST";

        /**
         * <h2>Key Access to the display name of the user's list of functional alias   </h2>
         * <p>Response: List<String> that contains the functional alias display name of every available functional alias </p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcpttUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcpttUserConf}</br> </p>
         * <p>ConfigurationInfoMcdataUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcdataUserConf}</br> </p>
         * <p>ConfigurationInfoMcvideoUserConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcvideoUserConf}</p>
         */
        public static final String USER_FUNCTIONAL_ALIAS_DISPLAY_NAME_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".USER_FUNCTIONAL_ALIAS_DISPLAY_NAME_LIST";

        /**
         * <h2>Key Access to the list of functional alias uris available in the whole MCPTT Service  </h2>
         * <p>Response: List<String> that contains the functional alias identifier of each available functional alias </p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcpttServiceConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcpttServiceConf}</br> </p>
         * <p>ConfigurationInfoMcdataServiceConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcdataServiceConf}</br> </p>
         * <p>ConfigurationInfoMcvideoServiceConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcvideoServiceConf}</p>
         */
        public static final String SERVICE_FUNCTIONAL_ALIAS_URI_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".SERVICE_FUNCTIONAL_ALIAS_URI_LIST";

        /**
         * <h2>Key Access to the display name of the whole service functional alias  </h2>
         * <p>Response: List<String> that contains the functional alias display name of each available functional alias </p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoMcpttServiceConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcpttServiceConf}</br> </p>
         * <p>ConfigurationInfoMcdataServiceConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcdataServiceConf}</br> </p>
         * <p>ConfigurationInfoMcvideoServiceConf {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoMcvideoServiceConf}</p>
         */
        public static final String SERVICE_FUNCTIONAL_ALIAS_DISPLAY_NAME_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".SERVICE_FUNCTIONAL_ALIAS_DISPLAY_NAME_LIST";

        /**
         * <h2>Key Access to the error codes associated to the configuration events.</h2>
         * <p>Response: Integer indicating the error code.</p>
         * <p>The configuration event types in which this tag can be present are: </br> </p>
         * <p>ConfigurationInfoError {@link org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.ConfigurationInfoEventTypeEnum#ConfigurationInfoError}</p>
         * <p>Values:</p>
         * <p>0 means no errors.</p>
         * <p>Code	Explanatory text	Description</p>
         * <p>101	It has been impossible to obtain authentication data</p>
         *
         */
        public static final String ERROR_CODE = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".ERROR_CODE";

        /**
         * <h2>Key Access to error string associated to the configuration events.</h2>
         * <p>Response: String describing the error.</p>
         */
        public static final String ERROR_STRING = "org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras" + ".ERROR_STRING";

        /**
         * @enum McpttAllowTypeEnum
         * @brief Enumeration of all possible user permissions for MCPTT service
         *
         * <h2>Defines the different user permission that affect the user related to MCPTT user configuration.</h2>
         */
        public enum McpttAllowTypeEnum {
            none(0x00),  /*!< invalid permission flag.*/
            privateCallAllow(0x01),  /*!< Indicates that the user is allowed to make private calls*/
            emergencyGroupCallAllow(0x01 << 1), /*!< Indicates that the user is allowed to make emergency group calls*/
            emergencyPrivateCallAllow(0x01 << 2), /*!< Indicates that the user is allowed to make emergency private calls*/
            imminentPerilCallAllow(0x01 << 3), /*!< Indicates that the user is allowed to make imminent peril calls*/
            emergencyAlertActivateAllow(0x01 << 4), /*!< Indicates that the user is allowed to activate emergency alerts*/
            emergencyGroupCancelAllow(0x01 << 5), /*!< Indicates that the  user is authorised to cancel an emergency group call */
            emergencyPrivateCallCancelAllow(0x01 << 6), /*!< Indicates that the user is authorised to cancel an emergency priority in an emergency private call */
            imminentPerilCallCancelAllow(0x01 << 7), /*!< Indicates that the user is authorised to cancel an imminent peril group call */
            emergencyAlertCancelAllow(0x01 << 8), /*!< Indicates that the user is authorised to cancel an emergency alert  */
            requestTargetUserAffiliationStatusAllow(0x01 << 9), /*!< Indicates that the user is authorised to request the list of groups to which a specified user is affiliated. */
            requestTargetUserAffiliationRequestAllow(0x01 << 10), /*!< Indicates that the  user is authorised to request specified user(s) to be affiliated to/deaffiliated from specified group(s).  */
            requestTargetUserAffiliationRecommendationAllow(0x01 << 11), /*!< Indicates that the user is authorised to recommend to specified user(s) to affiliate to specified group(s). */
            privateCallToAnyUserAllow(0x01 << 12),   /*!< Indicates that the user is authorised to request a private call to any users. */
            regroupingAllow(0x01 << 13),  /*!< Indicates that the  user is locally authorised to send a dynamic regrouping request  */
            privateCallCallbackRequestAllow(0x01 << 14),  /*!< Indicates that the  user is authorised to request a private call call-back */
            privateCallCallbackCancelAllow(0x01 << 15),  /*!< Indicates that the  user is authorised to cancel a private call call-back cancel */
            remotelyInitiatedAmbientListeningCallAllow(0x01 << 16),  /*!<  Indicates that the  user is authorised to request a remote initiated ambient listening call */
            locallyInitiatedAmbientListeningCallAllow(0x01 << 17),  /*!< Indicates that the  user is authorised to request a locally initiated ambient listening call */
            firstToAnswerCallAllow(0x01 << 18),  /*!< Indicates that the  user is authorised to request a first-to-answer call */
            functionalAliasTakeOverAllow(0x01 << 19);  /*!< Indicates that the  user is authorised to take over the functional alias(es) previously activated by another user */

            private int code;

            McpttAllowTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            public static List<McpttAllowTypeEnum> getListAllowType(int num) {
                ArrayList<McpttAllowTypeEnum> allowTypeEnums = new ArrayList<>();
                for (ConfigurationInfoEventExtras.McpttAllowTypeEnum allowTypeEnum : ConfigurationInfoEventExtras.McpttAllowTypeEnum
                        .values()) {
                    if ((num & allowTypeEnum.getValue()) == allowTypeEnum.getValue()) {
                        allowTypeEnums.add(allowTypeEnum);
                    }
                }

                return allowTypeEnums;
            }

            public static int getValue(List<McpttAllowTypeEnum> allowTypeEnums) {
                int value = none.getValue();
                for (ConfigurationInfoEventExtras.McpttAllowTypeEnum allowTypeEnum : allowTypeEnums) {
                    if (allowTypeEnum != null) {
                        value += allowTypeEnum.getValue();
                    }
                }

                return value;
            }
        }

        /**
         * @enum McdataAllowTypeEnum
         * @brief Enumeration of all possible user permissions for MCData service
         *
         * <h2>Defines the different user permission that affect the user related to MCData user configuration.</h2>
         */
        public enum McdataAllowTypeEnum {
            none(0x00),  /*!< invalid permission flag.*/
            emergencyAlertActivateAllow(0x01 << 1), /*!< Indicates that the user is allowed to activate emergency alerts */
            emergencyAlertCancelAllow(0x01 << 2), /*!< Indicates that the user is authorised to cancel an emergency alert */
            emergencyAlertCancelAnyUserAllow(0x01 << 3), /*!< Indicates that the  user is authorised to cancel any on-network emergency alert on any MCData UE of any user */
            requestTargetUserAffiliationStatusAllow(0x01 << 4), /*!< Indicates that the user is authorised to request the list of groups to which a specified user is affiliated. */
            requestTargetUserAffiliationRequestAllow(0x01 << 5), /*!< Indicates that the  user is authorised to request specified user(s) to be affiliated to/deaffiliated from specified group(s).  */
            requestTargetUserAffiliationRecommendationAllow(0x01 << 6), /*!< Indicates that the user is authorised to recommend to specified user(s) to affiliate to specified group(s). */
            regroupingAllow(0x01 << 7),  /*!< Indicates that the  user is locally authorised to send a dynamic regrouping request */
            transmitDataAllow(0x01 << 8);  /*!< Indicates that the user is permitted to transmit data. */

            private int code;

            McdataAllowTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            public static List<McdataAllowTypeEnum> getListAllowType(int num) {
                ArrayList<McdataAllowTypeEnum> allowTypeEnums = new ArrayList<>();
                for (ConfigurationInfoEventExtras.McdataAllowTypeEnum allowTypeEnum : ConfigurationInfoEventExtras.McdataAllowTypeEnum
                        .values()) {
                    if ((num & allowTypeEnum.getValue()) == allowTypeEnum.getValue()) {
                        allowTypeEnums.add(allowTypeEnum);
                    }
                }

                return allowTypeEnums;
            }

            public static int getValue(List<McdataAllowTypeEnum> allowTypeEnums) {
                int value = none.getValue();
                for (ConfigurationInfoEventExtras.McdataAllowTypeEnum allowTypeEnum : allowTypeEnums) {
                    if (allowTypeEnum != null) {
                        value += allowTypeEnum.getValue();
                    }
                }

                return value;
            }
        }

        /**
         * @enum McvideoAllowTypeEnum
         * @brief Enumeration of all possible user permissions for Mcvideo service
         *
         * <h2>Defines the different user permission that affect the user related to Mcvideo user configuration.</h2>
         */
        public enum McvideoAllowTypeEnum {
            none(0x00),  /*!< invalid permission flag.*/
            privateCallAllow(0x01),  /*!< Indicates that the user is allowed to make private calls*/
            emergencyGroupCallAllow(0x01 << 1), /*!< Indicates that the user is allowed to make emergency group calls*/
            emergencyPrivateCallAllow(0x01 << 2), /*!< Indicates that the user is allowed to make emergency private calls*/
            imminentPerilCallAllow(0x01 << 3), /*!< Indicates that the user is allowed to make imminent peril calls*/
            emergencyAlertActivateAllow(0x01 << 4), /*!< Indicates that the user is allowed to activate emergency alerts*/
            emergencyGroupCancelAllow(0x01 << 5), /*!< Indicates that the  user is authorised to cancel an emergency group call */
            emergencyPrivateCallCancelAllow(0x01 << 6), /*!< Indicates that the user is authorised to cancel an emergency priority in an emergency private call */
            imminentPerilCallCancelAllow(0x01 << 7), /*!< Indicates that the user is authorised to cancel an imminent peril group call */
            emergencyAlertCancelAllow(0x01 << 8), /*!< Indicates that the user is authorised to cancel an emergency alert  */
            requestTargetUserAffiliationStatusAllow(0x01 << 9), /*!< Indicates that the user is authorised to request the list of groups to which a specified user is affiliated. */
            requestTargetUserAffiliationRequestAllow(0x01 << 10), /*!< Indicates that the  user is authorised to request specified user(s) to be affiliated to/deaffiliated from specified group(s).  */
            requestTargetUserAffiliationRecommendationAllow(0x01 << 11), /*!< Indicates that the user is authorised to recommend to specified user(s) to affiliate to specified group(s). */
            privateCallToAnyUserAllow(0x01 << 12),   /*!< Indicates that the user is authorised to request a private call to any users. */
            regroupingAllow(0x01 << 13),  /*!< Indicates that the  user is locally authorised to send a dynamic regrouping request  */
            remotelyInitiatedAmbientViewingCallAllow(0x01 << 14),  /*!<  Indicates that the  user is authorised to request a remote initiated ambient viewing call */
            locallyInitiatedAmbientViewingCallAllow(0x01 << 15),  /*!< Indicates that the  user is authorised to request a locally initiated ambient viewing call */
            videoPushAllow(0x01 << 16);  /*!< Indicates that the MCVideo user is authorised to push a video to another MCVideo user. */

            private int code;

            McvideoAllowTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            public static List<McvideoAllowTypeEnum> getListAllowType(int num) {
                ArrayList<McvideoAllowTypeEnum> allowTypeEnums = new ArrayList<>();
                for (ConfigurationInfoEventExtras.McvideoAllowTypeEnum allowTypeEnum : ConfigurationInfoEventExtras.McvideoAllowTypeEnum
                        .values()) {
                    if ((num & allowTypeEnum.getValue()) == allowTypeEnum.getValue()) {
                        allowTypeEnums.add(allowTypeEnum);
                    }
                }

                return allowTypeEnums;
            }

            public static int getValue(List<McvideoAllowTypeEnum> allowTypeEnums) {
                int value = none.getValue();
                for (ConfigurationInfoEventExtras.McvideoAllowTypeEnum allowTypeEnum : allowTypeEnums) {
                    if (allowTypeEnum != null) {
                        value += allowTypeEnum.getValue();
                    }
                }

                return value;
            }
        }

        /**
         * @enum ConfigurationInfoEventTypeEnum
         * @brief Enumeration of all possible configuration events
         *
         * <h2>Defines the different events that can occur related to MCS service configuration.</h2>
         */
        public enum ConfigurationInfoEventTypeEnum {
            ConfigurationInfoNone(""),
            ConfigurationInfoError("org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.CONFIGURATION_UPDATE_EVENT_TYPE" + ".ConfigurationInfoError"), /*!< configuration information error.*/
            ConfigurationInfoMcpttUserConf("org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.CONFIGURATION_UPDATE_EVENT_TYPE" + ".ConfigurationInfoMcpttUserConf"), /*!< MCPTT User configuration information update.*/
            ConfigurationInfoMcdataUserConf("org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.CONFIGURATION_UPDATE_EVENT_TYPE" + ".ConfigurationInfoMcdataUserConf"), /*!< MCData User configuration information update.*/
            ConfigurationInfoMcvideoUserConf("org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.CONFIGURATION_UPDATE_EVENT_TYPE" + ".ConfigurationInfoMcvideoUserConf"), /*!< MCVideo User configuration information update.*/
            ConfigurationInfoMcpttServiceConf("org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.CONFIGURATION_UPDATE_EVENT_TYPE" + ".ConfigurationInfoMcpttServiceConf"), /*!< MCPTT Service configuration information update.*/
            ConfigurationInfoMcdataServiceConf("org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.CONFIGURATION_UPDATE_EVENT_TYPE" + ".ConfigurationInfoMcdataServiceConf"), /*!< MCData Service configuration information update.*/
            ConfigurationInfoMcvideoServiceConf("org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.CONFIGURATION_UPDATE_EVENT_TYPE" + ".ConfigurationInfoMcvideoServiceConf"), /*!< MCVideo Service configuration information update.*/
            ConfigurationInfoMcpttPrivateContacts("org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.CONFIGURATION_UPDATE_EVENT_TYPE" + ".ConfigurationInfoMcpttPrivateContacts"),  /*!< MCPTT private contacts configuration information update. This information is exchanged in another event due to its size*/
            ConfigurationInfoMcdataPrivateContacts("org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.CONFIGURATION_UPDATE_EVENT_TYPE" + ".ConfigurationInfoMcdataPrivateContacts"), /*!< MCData private contacts configuration information update. This information is exchanged in another event due to its size */
            ConfigurationInfoMcvideoPrivateContacts("org.mcopenplatform.muoapi.ConstantsMCOP.ConfigurationInfoEventExtras.CONFIGURATION_UPDATE_EVENT_TYPE" + ".ConfigurationInfoMcvideoPrivateContacts"); /*!< MCVideo private contacts configuration information update. This information is exchanged in another event due to its size */

            private final String text;

            /**
             * @param text
             */
            ConfigurationInfoEventTypeEnum(final String text) {
                this.text = text;
            }

            /* (non-Javadoc)
             * @see java.lang.Enum#toString()
             */
            @Override
            public String toString() {
                return text;
            }

            public static ConfigurationInfoEventTypeEnum fromString(String text) {
                for (ConfigurationInfoEventTypeEnum value : ConfigurationInfoEventTypeEnum.values()) {
                    if (value.text.equalsIgnoreCase(text)) {
                        return value;
                    }
                }
                return null;
            }
        }
    }

    /**
     * @brief Group information related related event. Manages the events regarding group information changes
     *
     * <h1>Key Access to the values of {@link ConstantsMCOP.ActionsCallBack#groupInfoEvent} event types </h1>
     * <p>This class contains all the answers to the actions of method {@link IMCOPsdk#getGroupsInfo()}  </p>
     */
    public static class GroupInfoEventExtras {

        /**
         * <h2>Key Access to the group information event type. See {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum} for all possible values.</h2>
         * <p>Response: Integer type value </p>
         * <p>This parameter is included in all call type events {@link ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum}</p>
         */
        public static final String EVENT_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".EVENT_TYPE";

        /**
         * <h2>Key Access to the identity of the group causing the event.</h2>
         * <p>Response: String type value representing the identifier </p>
         * <p>This parameter is included in all call type events {@link ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum}</p>
         */
        public static final String GROUP_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".GROUP_ID";

        /**
         * <h2>Key Access to the Display Name that allows to distinguish the MCPTT group causing the event.</h2>
         * <p>Response: String type value representing the identifier </p>
         * <p>The group information event types in which this tag can be present are: </br> </p>
         * <p>GroupInfoEventNew {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventNew}</br> </p>
         * <p>GroupInfoEventUpdate {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventUpdate}</p>
         */
        public static final String DISPLAY_NAME = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".DISPLAY_NAME";

        /**
         * <h2>Key Access to the permissions of the group.</h2>
         * <p>Response: Integer containing the different FLAGs of the user's permissions on the group.  See {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.AllowTypeEnum} for all possible values.</p>
         * <p>The group information event types in which this tag can be present are: </br> </p>
         * <p>GroupInfoEventNew {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventNew}</br> </p>
         * <p>GroupInfoEventUpdate {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventUpdate}</p>
         */
        public static final String ALLOWS_GROUP = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".ALLOWS_GROUP";

        /**
         * <h2>Key Access to maximum allowed size for SDS data transmission.</h2>
         * <p>Indicates the maximum size of data (in bytes) that the originating MCData client is allowed to send to the MCData server for on-network SDS communications.</p>
         * <p>Response: Integer (in bytes)</p>
         * <p>The group information event types in which this tag can be present are: </br> </p>
         * <p>GroupInfoEventNew {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventNew}</br> </p>
         * <p>GroupInfoEventUpdate {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventUpdate}</p>
         */
        public static final String MAX_DATA_SIZE_FOR_SDS = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".MAX_DATA_SIZE_FOR_SDS";

        /**
         * <h2>Key Access to maximum allowed file size for FD data transmission.</h2>
         * <p>Indicates the maximum size of data (in bytes) that the originating MCData client is allowed to send to the MCData server for on-network FD communications.</p>
         * <p>Response: Integer (in bytes)</p>
         * <p>The group information event types in which this tag can be present are: </br> </p>
         * <p>GroupInfoEventNew {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventNew}</br> </p>
         * <p>GroupInfoEventUpdate {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventUpdate}</p>
         */

        public static final String MAX_DATA_SIZE_FOR_FD = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".MAX_DATA_SIZE_FOR_FD";

        /**
         * <h2>Key Access to maximum allowed file size for FD data transmission using HTTP.</h2>
         * <p>Indicates the maximum size of data (in bytes) which the MCData server always requests the terminating MCData client to automatically download for on-network FD communications using HTTP.</p>
         * <p>Response: Integer (in bytes)</p>
         * <p>The group information event types in which this tag can be present are: </br> </p>
         * <p>GroupInfoEventNew {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventNew}</br> </p>
         * <p>GroupInfoEventUpdate {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventUpdate}</p>
         */
        public static final String MAX_DATA_SIZE_AUTO_RECV = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".MAX_DATA_SIZE_AUTO_RECV";

        /**
         * <h2>Key Access to real-time video mode.</h2>
         * <p>Response: String indicating the real-time video mode. See {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.ActionRealTimeVideoType} for all possible values.</p>
         * <p>The group information event types in which this tag can be present are: </br> </p>
         * <p>GroupInfoEventNew {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventNew}</br> </p>
         * <p>GroupInfoEventUpdate {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventUpdate}</p>
         */
        public static final String ACTIVE_REAL_TIME_VIDEO_MODE = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".ACTIVE_REAL_TIME_VIDEO_MODE";

        /**
         * @enum ActionRealTimeVideoType
         * @brief Enumeration of all possible real-time video modes
         *
         * <h2>Defines the different types of real-time video modes.</h2>
         */
        public enum ActionRealTimeVideoType {
            none(""), /*!< invalid real time video type.*/
            urgentRealTime("urgent-real-time"), /*!< urgent and real time video type.*/
            nonUrgentRealTime("non-urgent-real-time"), /*!< not urgent but real time video type.*/
            nonRealTime("non-real-time"); /*!< non urgent nor real time video type */

            private final String text;

            /**
             * @param text
             */
            ActionRealTimeVideoType(final String text) {
                this.text = text;
            }

            /* (non-Javadoc)
             * @see java.lang.Enum#toString()
             */
            @Override
            public String toString() {
                return text;
            }
        }

        /**
         * <h2>Key Access to the list of the identifiers of the users that are part of the group.</h2>
         * <p> Response: String list containing the UserID unequivocally identifies each user. (List<String>) </p>
         * <p>The group information event types in which this tag can be present are: </br> </p>
         * <p>GroupInfoEventNew {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventNew}</br> </p>
         * <p>GroupInfoEventUpdate {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventUpdate}</p>
         */
        public static final String PARTICIPANTS_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".PARTICIPANTS_LIST";

        /**
         * <h2>Key Access to the list of the display names of the users that are part of the group.</h2>
         * <p> Response: String list containing the display name that identifies each user in a more pleasant way. (List<String>) </p>
         * <p>The group information event types in which this tag can be present are: </br> </p>
         * <p>GroupInfoEventNew {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventNew}</br> </p>
         * <p>GroupInfoEventUpdate {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventUpdate}</p>
         */
        public static final String PARTICIPANTS_LIST_DISPLAY_NAME = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".PARTICIPANTS_LIST_DISPLAY_NAME";

        /**
         * <h2>Key Access to the list of the participant type of the users that are part of the group.</h2>
         * <p> Response: String list containing the participant type that defines the place of each user inside the organization. (List<String>) </p>
         * <p>The group information event types in which this tag can be present are: </br> </p>
         * <p>GroupInfoEventNew {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventNew}</br> </p>
         * <p>GroupInfoEventUpdate {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventUpdate}</p>
         */
        public static final String PARTICIPANTS_LIST_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".PARTICIPANTS_LIST_TYPE";

        /**
         * <h2>Key Access to the list of the participant type of the users that are part of the group. Not defined by the 3gpp. </h2>
         * <p> Response: String list containing the user priority of each user inside the organization. (List<String>) </p>
         * <p>The group information event types in which this tag can be present are: </br> </p>
         * <p>GroupInfoEventNew {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventNew}</br> </p>
         * <p>GroupInfoEventUpdate {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventUpdate}</p>
         */
        public static final String PARTICIPANTS_USER_PRIORITY = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".PARTICIPANTS_USER_PRIORITY";

        /**
         * <h2>Key Access to the group priority </h2>
         * <p> Response: integer value with the group priority level </p>
         * <p>The group information event types in which this tag can be present are: </br> </p>
         * <p>GroupInfoEventNew {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventNew}</br> </p>
         * <p>GroupInfoEventUpdate {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventUpdate}</p>
         */
        public static final String GROUP_PRIORITY = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".GROUP_PRIORITY";

        /**
         * <h2>Key Access to the error codes associated to the group info events.</h2>
         * <p>Response: Integer indicating error codes.</p>
         * <p>Values:</p>
         * <p>0 means no errors.</p>
         * <p> 101   Received data groups not valid</p>
         */
        public static final String ERROR_CODE = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".ERROR_CODE";

        /**
         * <h2>Key Access to error string associated to the group information events.</h2>
         * <p>Response: String describing the error.</p>
         */
        public static final String ERROR_STRING = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".ERROR_STRING";

        /**
         * <h2>Key Access to the temporary group ID that the current group belongs to</h2>
         * <p>Response: String with URI format that identifies the temporary group.</p>
         * <p>The group information event types in which this tag can be present are: </br> </p>
         * <p>GroupInfoEventNew {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventNew}</br> </p>
         * <p>GroupInfoEventUpdate {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEventUpdate}</p>
         */
        public static final String REGROUP_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".REGROUP_ID";

        /**
         * <h2>Key Access to the emergency status of the specified group </h2>
         * <p>Response: Integer identifying the emergency status type. See {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEmergencyStatusTypeEnum} for all possible values. </p>
         * <p>The group information event types in which this tag can be present are: </br> </p>
         * <p>GroupInfoEmergencyStatusUpdate {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEmergencyStatusUpdate}</p>
         */
        public static final String EMERGENCY_STATUS = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".EMERGENCY_STATUS";

        /**
         * <h2>Key Access to the the MCX Service ID of the user related to the remote group selection events </h2>
         * <p>Response: String type value representing the identifier </p>
         * <p>The group information event types in which this tag can be present are: </br> </p>
         * <p>GroupInfoIncomingRemoteSelection {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoIncomingRemoteSelection}</br> </p>
         * <p>GroupInfoRemoteSelectionSuccess {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoRemoteSelectionSuccess}</br> </p>
         * <p>GroupInfoRemoteSelectionSuccess {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoRemoteSelectionError}</p>
         */
        public static final String REMOTE_USER_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".REMOTE_USER_ID";

        /**
         * <h2> Key Access to the group operation service identification tag.</h2>
         * <p> Indicates the service or services for which the group operation is performed<p>
         * <p> It can be used in group operation request such as {@link IMCOPsdk#changeRemoteGroup(String, String, Map)} (String, int, Map)}, {@link IMCOPsdk#setGroupEmergencyAlertState(String, boolean, Map)} </p>
         * <p>The group info event types in which this tag can be present are: </br> </p>
         * <p>GroupInfoEmergencyStatusUpdate {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoEmergencyStatusUpdate}</br> </p>
         * <p>GroupInfoIncomingRemoteSelection {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoIncomingRemoteSelection}</br> </p>
         * <p>GroupInfoRemoteSelectionSuccess {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoRemoteSelectionSuccess}</br> </p>
         * <p>GroupInfoRemoteSelectionSuccess {@link org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras.GroupInfoEventTypeEnum#GroupInfoRemoteSelectionError}</br> </p>
         * <p> Contains a integer value that represents the service type. See {@link org.mcopenplatform.muoapi.ConstantsMCOP.ServiceTypeEnum} for all possible values.  </p>
         */
        public static final String TAG_GROUP_SERVICES = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupInfoEventExtras" + ".TAG_GROUP_SERVICES";

        /**
         * @enum AllowTypeEnum
         * @brief Enumeration of all possible group permissions
         *
         * <h2>Defines the different types of permissions in a group. If the allow value includes the corresponding flag, the permission is granted for the group.</h2>
         */
        public enum AllowTypeEnum {
            none(0x00), /*!< invalid group permission flag.*/
            //MCPTT
            mcpttInviteMembersAllow(0x01), /*!< Indicates if the group is allowed to invite group members when establishing the call. Differentiates prearranged from chat groups */
            mcpttEmergencyCallAllow(0x01 << 2),  /*!< Indicates that the identity is allowed to request an MCPTT-emergency call on the group */
            mcpttImminentPerilCallAllow(0x01 << 3), /*!< Indicates that the identity is allowed to request an MCPTT imminent peril call on the group */
            mcpttEmergencyAlertAllow(0x01 << 4), /*!< Indicates that the identity is allowed to request an MCPTT-emergency alert on the group */
            mcpttMediaProtectionAllow(0x01 << 17), /*!< Indicates that a GMK is required to confidentiality and integrity protect media */
            floorControlSignallingProtectionAllow(0x01 << 18), /*!< Indicates that both confidentiality protection and integrity protection of floor control signalling are require */
            //MCVIDEO
            mcVideoInviteMembersAllow(0x01 << 5), /*!< Indicates if the group is allowed to invite group members when establishing the call. Differentiates prearranged from chat groups */
            mcVideoEmergencyCallAllow(0x01 << 6), /*!< Indicates that the identity is allowed to request an MCVideo-emergency call on the group */
            mcVideoImminentPerilCallAllow(0x01 << 7), /*!< Indicates that the identity is allowed to request an MCVideo imminent peril call on the group */
            mcVideoEmergencyAlertAllow(0x01 << 8), /*!< Indicates that the identity is allowed to request an MCVideo-emergency alert on the group */
            mcVideoMediaProtectionAllow(0x01 << 21), /*!< Indicates that a GMK is required to confidentiality and integrity protect media */
            mcVideoTransmissionControlSignallingProtectionAllow(0x01 << 22), /*!< Indicates that both confidentiality protection and integrity protection of transmission control signalling are require */
            //MCDATA
            shortDataServiceAllow(0x01 << 11), /*!< Indicates that short data service is enabled for the MCData group */
            fileDistributionAllow(0x01 << 12), /*!< Indicates that file distribution is enabled for the MCData group */
            conversationManagementAllow(0x01 << 13), /*!< Indicates that conversation management is enabled for the MCData group */
            txControlAllow(0x01 << 14), /*!< Indicates that transmission control is enabled for the MCData group */
            rxControlAllow(0x01 << 15), /*!< Indicates that reception control is enabled for the MCData group */
            enhancedStatusAllow(0x01 << 16), /*!< Indicates that enhanced status is enabled for the MCData group */
            mcDataMediaProtectionAllow(0x01 << 19), /*!< Indicates that a GMK is required to confidentiality and integrity protect media */
            mcDataTransmissionControlSignallingProtectionAllow(0x01 << 20), /*!< Indicates that both confidentiality protection and integrity protection of transmission control signalling are require */
            dataTransmissionAllowed(0x01 << 9); /*!< Indicates that the identity is allowed to transmit data in this group */

            private int code;

            AllowTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            public static List<AllowTypeEnum> getListAllowType(int num) {
                ArrayList<AllowTypeEnum> allowTypeEnums = new ArrayList<>();
                for (AllowTypeEnum allowTypeEnum : AllowTypeEnum.values()) {
                    if ((num & allowTypeEnum.getValue()) == allowTypeEnum.getValue()) {
                        allowTypeEnums.add(allowTypeEnum);
                    }
                }

                return allowTypeEnums;
            }

            public static int getValue(List<AllowTypeEnum> allowTypeEnum) {
                int value = none.getValue();
                for (AllowTypeEnum allowTypeEnum1 : allowTypeEnum) {
                    if (allowTypeEnum1 != null) {
                        value += allowTypeEnum1.getValue();
                    }
                }

                return value;
            }
        }

        /**
         * @enum GroupInfoEventTypeEnum
         * @brief Enumeration of all possible group information related events
         *
         * <h2>Defines the different events that can occur related to MCS service registration.</h2>
         */
        public enum GroupInfoEventTypeEnum {
            GroupInfoNone(0x00), /*!< Invalid group info event */
            GroupInfoEventError(0x01),/*!< Error event */
            GroupInfoEventNew(0x02), /*!< New group info event */
            GroupInfoEventUpdate(0x03), /*!< Update an already notified group info */
            GroupInfoEventDeletion(0x04), /*!< Delete info related to a specific group */
            GroupInfoIncomingRemoteSelection(0x05), /*!< Incoming request to change selected group */
            GroupInfoRemoteSelectionError(0x06), /*!< Indicates that a request to change the selected group of another user has failed */
            GroupInfoRemoteSelectionSuccess(0x07), /*!< Indicates that a request to change the selected group of another user has been sent successfully */
            GroupInfoEmergencyStatusUpdate(0x08), /*!< group emergency status changed */
            GroupInfoDynamicData(0x09);  /*!< Dynamic group info, not retrieved from configuration files */

            private int code;

            GroupInfoEventTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (GroupInfoEventExtras.GroupInfoEventTypeEnum pageType : GroupInfoEventExtras.GroupInfoEventTypeEnum.values()) {
                    map.put(pageType.code, pageType);
                }
            }

            public static GroupInfoEventExtras.GroupInfoEventTypeEnum fromInt(int pageType) {
                return (GroupInfoEventExtras.GroupInfoEventTypeEnum) map.get(pageType);
            }
        }

        /**
         * @enum GroupInfoEmergencyStatusTypeEnum
         * @brief Enumeration of all possible group emergency status types
         *
         * <h2>Defines the different emergency group status types.</h2>
         */
        public enum GroupInfoEmergencyStatusTypeEnum {
            GroupInfoEmergencyStatusNone(0x00), /*!< Invalid emergency status type */
            GroupInfoEmergencyStatusNormal(0x01), /*!< group normal status */
            GroupInfoEmergencyStatusImminentPeril(0x02), /*!< group imminent peril status */
            GroupInfoEmergencyStatusEmergency(0x03); /*!< group emergency status */

            private int code;

            GroupInfoEmergencyStatusTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (GroupInfoEmergencyStatusTypeEnum pageType : GroupInfoEmergencyStatusTypeEnum.values()) {
                    map.put(pageType.code, pageType);
                }
            }

            public static GroupInfoEmergencyStatusTypeEnum fromInt(int pageType) {
                return (GroupInfoEmergencyStatusTypeEnum) map.get(pageType);
            }
        }

        /**
         * @enum GroupOperationRequestTypeEnum
         * @brief Enumeration of all possible group related operation request types
         *
         * <h2>Defines the different group operation request types.</h2>
         */
        public enum GroupOperationRequestTypeEnum {
            GroupOperationRequestGroupCreation(0x00), /*!< group creation request */
            GroupOperationRequestGroupUpdate(0x01), /*!< group update request */
            GroupOperationRequestGroupDeletion(0x02), /*!< group deletion request */
            GroupOperationRequestTempGroupFormation(0x03), /*!< temporary group formation request */
            GroupOperationRequestTempGroupUpdate(0x04), /*!< temporary group update request */
            GroupOperationRequestTempGroupTearDown(0x05); /*!< temporary group tear down request */

            private int code;

            GroupOperationRequestTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (GroupOperationRequestTypeEnum pageType : GroupOperationRequestTypeEnum.values()) {
                    map.put(pageType.code, pageType);
                }
            }

            public static GroupOperationRequestTypeEnum fromInt(int pageType) {
                return (GroupOperationRequestTypeEnum) map.get(pageType);
            }
        }

    }

    /**
     * @brief Manages the events regarding the affiliation process
     *
     * <h1>Key Access to the values of {@link ConstantsMCOP.ActionsCallBack#groupAffiliationEvent} event types </h1>
     * <p>This class contains all the answers to the actions of method {@link IMCOPsdk#getGroupsAffiliation()} </p>
     */
    public static class GroupAffiliationEventExtras {

        /**
         * <h2>Key Access to the group affiliation event type. See {@link AffiliationEventTypeEnum} for all possible values.</h2>
         * <p>Response: Integer type value </p>
         */
        public static final String EVENT_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupAffiliationEventExtras" + ".EVENT_TYPE";

        /**
         * <h2>Key Access to the group ID for which the affiliation operation or status change is being performed. </h2>
         * <p>Response: String type value representing the identifier </p>
         * <p>The affiliation event types in which this tag can be present are: </br> </p>
         * <p>GroupAffiliationError {@link AffiliationEventTypeEnum#AffiliationError}</p>
         * <p>GroupAffiliationIncomingRemoteAction {@link AffiliationEventTypeEnum#AffiliationIncomingRemoteAction}</p>
         * <p>GroupAffiliationRemoteActionSuccess {@link AffiliationEventTypeEnum#AffiliationRemoteActionSuccess}</p>
         */
        public static final String GROUP_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupAffiliationEventExtras" + ".GROUP_ID";

        /**
         * <h2>Key Access to the list of groups available for the user and its corresponding affiliation status </h2>
         * <p>Response: Map<String,{@link AffiliationStatusEnum}> that identifies the status of each group </p>
         * <p>The affiliation event types in which this tag can be present are: </br> </p>
         * <p> GroupAffiliationUpdate {@link AffiliationEventTypeEnum#AffiliationUpdate}</p>
         */
        public static final String GROUP_AFFILIATION_STATUS_MAP = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupAffiliationEventExtras" + ".GROUP_AFFILIATION_STATUS_MAP";

        /**
         * <h2>Key Access to the MCX Service ID of the user requesting that affiliation operation is carried out.</h2>
         * <p>Response: String with URI format that identifies the user.</p>
         * <p> It can be optionally used in group operation request as a tag {@link IMCOPsdk#groupManagementOperation(String, int, Map)} </p>
         * <p>The affiliation event types in which this tag can be present are: </br> </p>
         * <p> GroupAffiliationIncomingRemoteAction {@link AffiliationEventTypeEnum#AffiliationIncomingRemoteAction}</br> </p>
         * <p> GroupAffiliationRemoteActionSuccess {@link AffiliationEventTypeEnum#AffiliationRemoteActionSuccess}</p>
         */
        public static final String TAG_AFFILIATION_REMOTE_USER_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupAffiliationEventExtras" + ".TAG_REMOTE_USER_ID";

        /**
         * <h2>Key Access to the action type that is remotely requested to be performed.</h2>
         * <p>Response: Integer type value. See {@link AffiliationStatusEnum} for all possible values. </p>
         * <p>The affiliation event types in which this tag can be present are: </br> </p>
         * <p> GroupAffiliationIncomingRemoteAction {@link AffiliationEventTypeEnum#AffiliationIncomingRemoteAction}</p>
         */
        public static final String REMOTE_ACTION_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupAffiliationEventExtras" + ".REMOTE_ACTION_TYPE";

        /**
         * <h2>Key Access to the error codes associated to the group affiliation events.</h2>
         * <p>Response: Integer indicating the error code.</p>
         * <p>The affiliation event types in which this tag can be present are: </br> </p>
         * <p>GroupAffiliationError {@link AffiliationEventTypeEnum#AffiliationError}</br> </p>
         * <p>Values:</p>
         * <p>0 means no errors.</p>
         * <p>  101 Non-existent group</p>
         * <p>  102 Action not allowed</p>
         * <p>  103 The user is not a member of the group</p>
         * <p>  104 The group is not currently affiliate</p>
         * <p>  105 The group already is affiliated</p>
         * <p>  106 The group already is not affiliated</p>
         * <p>  107 The group does not belong to the list of existing groups</p>
         * <p>  108 We don't have information of affiliation</p>
         * <p>  109 The group already is affiliating or deaffiliating</p>
         * <p>  110 Max number group affiliated</p>
         */
        public static final String ERROR_CODE = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupAffiliationEventExtras" + ".ERROR_CODE";

        /**
         * <h2>Key Access to error string associated to the group affiliation events.</h2>
         * <p>Response: String describing the error.</p>
         */
        public static final String ERROR_STRING = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupAffiliationEventExtras" + ".ERROR_STRING";

        /**
         * <h2> Key Access to the group affiliation service identification tag.</h2>
         * <p> Response: Contains a integer value that represents the service type. See {@link org.mcopenplatform.muoapi.ConstantsMCOP.ServiceTypeEnum} for all possible values.  </p>
         * <p> Indicates the service or services for which the affiliation operation is performed<p>
         * <p> It can be optionally used in group operation request as a tag {@link IMCOPsdk#groupManagementOperation(String, int, Map)} </p>
         * <p>The group affiliation event types in which this tag can be present are: </br> </p>
         * <p> GroupAffiliationIncomingRemoteAction {@link AffiliationEventTypeEnum#AffiliationIncomingRemoteAction}</br> </p>
         * <p> GroupAffiliationUpdate {@link AffiliationEventTypeEnum#AffiliationUpdate}</br> </p>
         * <p> GroupAffiliationRemoteActionSuccess {@link AffiliationEventTypeEnum#AffiliationRemoteActionSuccess}</p>
         */
        public static final String TAG_AFFILIATION_SERVICES = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupAffiliationEventExtras" + ".TAG_AFFILIATION_SERVICES";

        /**
         * <h2> Key Access to the affiliation operation request on behalf of another user, using negotiated mode indication tag.</h2>
         * <p> Response:  It is a boolean. True means negotiated mode should be used.  </p>
         * <p> It can be optionally used in group operation request as a tag {@link IMCOPsdk#groupManagementOperation(String, int, Map)} </p>
         */
        public static final String TAG_AFFILIATION_REMOTE_NEGOTIATED_MODE = "org.mcopenplatform.muoapi.ConstantsMCOP.GroupAffiliationEventExtras" + ".TAG_AFFILIATION_REMOTE_NEGOTIATED_MODE";

        /**
         * @enum GroupAffiliationStatusEnum
         * @brief Enumeration of all possible affiliation status
         *
         * <h2>Defines the different status of the affiliation of a user to a group</h2>
         */
        public enum AffiliationStatusEnum {
            AffiliationStatusNone(0x00),  /*!< Invalid affiliation status*/
            AffiliationStatusDeaffiliated(0x01), /*!< Deaffiliated affiliation status. Indicates that the user is not affiliated to the group*/
            AffiliationStatusAffiliating(0x02), /*!< Affiliating affiliation status. Indicates that the user is being affiliated to the group*/
            AffiliationStatusAffiliated(0x03), /*!< Affiliated affiliation status. Indicates that the user is affiliated to the group*/
            AffiliationStatusDeaffiliating(0x04); /*!< Deaffiliating affiliation status. Indicates that the user is being deaffiliated to the group*/

            private int code;

            AffiliationStatusEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (AffiliationStatusEnum pageType : AffiliationStatusEnum.values()) {
                    map.put(pageType.code, pageType);
                }
            }

            public static AffiliationStatusEnum fromInt(int pageType) {
                return (AffiliationStatusEnum) map.get(pageType);
            }
        }

        /**
         * @enum GroupAffiliationEventTypeEnum
         * @brief Enumeration of all possible affiliation event types
         *
         * <h2>Defines the different events that can occur on performing an operation on the affiliation status</h2>
         */
        public enum AffiliationEventTypeEnum {
            AffiliationNone(0x00),  /*!< Invalid affiliation event*/
            AffiliationError(0x01), /*!< Error response event */
            AffiliationUpdate(0x02), /*!< Successful status update event */
            AffiliationIncomingRemoteAction(0x03), /*!< Incoming request to change affiliation status. This event occurs when another user tries to change the
                                                              affiliation on behalf of the user logged in the SDK, using negotiated mode. */
            AffiliationRemoteActionSuccess(0x04); /*!< Indicates that a request to change the affiliation on behalf of another user has been sent successfully */

            private int code;

            AffiliationEventTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (AffiliationEventTypeEnum pageType : AffiliationEventTypeEnum.values()) {
                    map.put(pageType.code, pageType);
                }
            }

            public static AffiliationEventTypeEnum fromInt(int pageType) {
                return (AffiliationEventTypeEnum) map.get(pageType);
            }
        }

        /**
         * @enum AffiliationRequestTypeEnum
         * @brief Enumeration of all possible affiliation request types
         *
         * <h2>Defines the different operations that can be performed to change affiliation status.</h2>
         */
        public enum AffiliationRequestTypeEnum {
            AffiliationRequestNone(0x00),  /*!< Invalid affiliation request type*/
            AffiliationRequestSelfAffiliate(0x01), /*!< Current user affiliation request */
            AffiliationRequestSelfDeaffiliate(0x02),  /*!< Current user deaffiliation request */
            AffiliationRequestTargetUserAffiliate(0x03), /*!< Target user affiliation request */
            AffiliationRequestTargetUserDeaffiliate(0x04); /*!< Target user deaffiliation request */

            private int code;

            AffiliationRequestTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (AffiliationRequestTypeEnum pageType : AffiliationRequestTypeEnum.values()) {
                    map.put(pageType.code, pageType);
                }
            }

            public static AffiliationRequestTypeEnum fromInt(int pageType) {
                return (AffiliationRequestTypeEnum) map.get(pageType);
            }
        }
    }

    /**
     * @brief Manages the events generated by functional alias operations.
     *
     * <h1>Key Access to the values of {@link ConstantsMCOP.ActionsCallBack#functionalAliasEvent} event types </h1>
     *
     */
    public static class FunctionalAliasEventExtras {

        /**
         * <h2>Key Access to the functional alias event type. See {@link org.mcopenplatform.muoapi.ConstantsMCOP.FunctionalAliasEventExtras.FunctionalAliasEventTypeEnum} for all possible values.</h2>
         * <p>Response: Integer type value </p>
         */
        public static final String EVENT_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.FunctionalAliasEventExtras" + ".EVENT_TYPE";

        /**
         * <h2>Key Access to the Functional Alias ID that allows to distinguish unequivocally the Functional Alias responsible of the event.</h2>
         * <p>Response: String that identifies the Functional Alias.</p>
         * <p>The functional alias event types in which this tag can be present are: </br> </p>
         * <p> FunctionalAliasOperationSuccess {@link ConstantsMCOP.FunctionalAliasEventExtras.FunctionalAliasEventTypeEnum#FunctionalAliasOperationSuccess}</br></p>
         * <p> FunctionalAliasOperationError {@link ConstantsMCOP.FunctionalAliasEventExtras.FunctionalAliasEventTypeEnum#FunctionalAliasOperationError}</p>
         */
        public static final String FUNCTIONAL_ALIAS_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.FunctionalAliasEventExtras" + ".FUNCTIONAL_ALIAS_ID";

        /**
         * <h2>Key Access to the response to method {@link org.mcopenplatform.muoapi.IMCOPsdk#functionalAliasOperation(String, int, Map)}</h2>
         * <p>Response: int matching {@link FunctionalAliasOperationTypeEnum}, indicates th type of the performed operation.</p>
         * <p>The functional alias event types in which this tag can be present are: </br> </p>
         * <p> FunctionalAliasOperationSuccess {@link ConstantsMCOP.FunctionalAliasEventExtras.FunctionalAliasEventTypeEnum#FunctionalAliasOperationSuccess} </br></p>
         * <p> FunctionalAliasOperationError {@link ConstantsMCOP.FunctionalAliasEventExtras.FunctionalAliasEventTypeEnum#FunctionalAliasOperationError} </p>
         */
        public static final String OPERATION_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.FunctionalAliasEventExtras" + ".OPERATION_TYPE";

        /**
         * <h2>Key Access to the list of functional alias available for the user and its corresponding status </h2>
         * <p>Response: Map<String,{@link FunctionalAliasStatusEnum}> that identifies the activation state of each functional alias</p>
         * <p>The functional alias event types in which this tag can be present are: </br> </p>
         * <p> FunctionalAliasStatusUpdate {@link ConstantsMCOP.FunctionalAliasEventExtras.FunctionalAliasEventTypeEnum#FunctionalAliasStatusUpdate}</p>
         */
        public static final String FUNCTIONAL_ALIAS_STATUS_MAP = "org.mcopenplatform.muoapi.ConstantsMCOP.FunctionalAliasEventExtras" + ".FUNCTIONAL_ALIAS_STATUS_MAP";

        /**
         * <h2>Key Access to the error codes associated to the functional alias events.</h2>
         * <p>Response: Integer indicating the error code.</p>
         * <p>The functional alias event types in which this tag can be present are: </br> </p>
         * <p>FunctionalAliasStatusError {@link org.mcopenplatform.muoapi.ConstantsMCOP.FunctionalAliasEventExtras.FunctionalAliasEventTypeEnum#FunctionalAliasStatusError}</br> </p>
         * <p>FunctionalAliasOperationError {@link org.mcopenplatform.muoapi.ConstantsMCOP.FunctionalAliasEventExtras.FunctionalAliasEventTypeEnum#FunctionalAliasOperationError}</br> </p>
         * <p>Values:</p>
         * <p>0 means no errors.</p>
         *
         */
        public static final String ERROR_CODE = "ConstantsMCOP.FunctionalAliasEventExtras" + ".ERROR_CODE";

        /**
         * <h2>Key Access to error string associated to the functional alias events.</h2>
         * <p>Response: String describing the error.</p>
         */
        public static final String ERROR_STRING = "org.mcopenplatform.muoapi.ConstantsMCOP.FunctionalAliasEventExtras" + ".ERROR_STRING";

        /**
         * <h2> Key Access to the flag indicating if it is wanted to set flag take over when requesting the activation of a functional alias..</h2>
         * <p> Response:  It is a boolean value. True means activate.  </p>
         * <p> It can be optionally used in functional alias operation request as a tag {@link IMCOPsdk#functionalAliasOperation(String, int, Map)} </p>
         */
        public static final String TAG_FUNCTIONAL_ALIAS_TAKE_OVER = "org.mcopenplatform.muoapi.ConstantsMCOP.FunctionalAliasEventExtras" + ".TAG_FUNCTIONAL_ALIAS_TAKE_OVER";

        /**
         * @enum FunctionalAliasStateEnum
         * @brief Enumeration of all possible functional alias status
         *
         * <h2>Defines the different possible status of a functional alias</h2>
         */
        public enum FunctionalAliasOperationTypeEnum {
            FunctionalAliasOperationNone(0x00),  /*!< Invalid functional alias operation */
            FunctionalAliasOperationActivate(0x01), /*!< Activate functional alias */
            FunctionalAliasOperationDeactivate(0x02); /*!< Deactivate functional alias */

            private int code;

            FunctionalAliasOperationTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (FunctionalAliasOperationTypeEnum pageType : FunctionalAliasOperationTypeEnum.values()) {
                    map.put(pageType.code, pageType);
                }
            }

            public static FunctionalAliasOperationTypeEnum fromInt(int pageType) {
                return (FunctionalAliasOperationTypeEnum) map.get(pageType);
            }
        }

        /**
         * @enum FunctionalAliasStatusEnum
         * @brief Enumeration of all possible functional alias status
         *
         * <h2>Defines the different possible status of a functional alias</h2>
         */
        public enum FunctionalAliasStatusEnum {
            FunctionalAliasStatusNone(0x00), /*!< Invalid functional alias status */
            FunctionalAliasStatusActivated(0x01), /*!< Functional alias activated */
            FunctionalAliasStatusActivating(0x02), /*!< Functional alias activation in progress */
            FunctionalAliasStatusDeactivated(0x03), /*!< Functional alias deactivated */
            FunctionalAliasStatusDeactivating(0x04), /*!< Functional alias deactivation in progress */
            FunctionalAliasStatusTakeOverPossible(0x05); /*!< Functional alias activated by another user, but a take-over can be performed if that functional alias wants to be activated */

            private int code;

            FunctionalAliasStatusEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (FunctionalAliasStatusEnum pageType : FunctionalAliasStatusEnum.values()) {
                    map.put(pageType.code, pageType);
                }
            }

            public static FunctionalAliasStatusEnum fromInt(int pageType) {
                return (FunctionalAliasStatusEnum) map.get(pageType);
            }
        }

        /**
         * @enum FunctionalAliasEventTypeEnum
         * @brief Enumeration of all possible functional alias event types
         *
         * <h2>Defines the different events that can occur related to functional alias</h2>
         */
        public enum FunctionalAliasEventTypeEnum {
            FunctionalAliasNone(0x00), /*!< Invalid functional alias event */
            FunctionalAliasStatusError(0x01), /*!< Error response event */
            FunctionalAliasStatusUpdate(0x02), /*!< Successful functional alias status update event*/
            FunctionalAliasOperationSuccess(0x03),  /*!< result of an activation/deactivation performed operation*/
            FunctionalAliasOperationError(0x04); /*!< result of an activation/deactivation performed operation*/
            private int code;

            FunctionalAliasEventTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (FunctionalAliasEventTypeEnum pageType : FunctionalAliasEventTypeEnum.values()) {
                    map.put(pageType.code, pageType);
                }
            }

            public static FunctionalAliasEventTypeEnum fromInt(int pageType) {
                return (FunctionalAliasEventTypeEnum) map.get(pageType);
            }
        }
    }

    /**
     * @brief Manages the events related to eMBMS operations
     *
     * <h1>Key Access to the values of {@link ConstantsMCOP.ActionsCallBack#eMBMSNotificationEvent} event types </h1>
     *
     */
    public static class EMBMSNotificationEventExtras {

        /**
         * <h2>Key Access to the eMBMS notification event type. See {@link org.mcopenplatform.muoapi.ConstantsMCOP.EMBMSNotificationEventExtras.EMBMSNotificationEventTypeEnum} for all possible values.</h2>
         * <p>Response: Integer type value </p>
         */
        public static final String EVENT_TYPE = "org.mcopenplatform.muoapi.ConstantsMCOP.EMBMSNotificationEventExtras" + ".EVENT_TYPE";

        /**
         * <h2>Key Access to the Temporary Mobile Group Identity (TMGI) identifying the eMBMS bearer.</h2>
         * <p>Response: String</p>
         * <p>The eMBMS event types in which this tag can be present are: </br> </p>
         * <p>EMBMSEventInCoverage {@link EMBMSNotificationEventTypeEnum#EMBMSEventInCoverage}</br> </p>
         * <p>EMBMSEventInUse {@link EMBMSNotificationEventTypeEnum#EMBMSEventInUse}</br> </p>
         * <p>EMBMSEventNotInUse {@link EMBMSNotificationEventTypeEnum#EMBMSEventNotInUse}</br> </p>
         * <p>EMBMSEventOutCoverage {@link EMBMSNotificationEventTypeEnum#EMBMSEventOutCoverage}</p>
         */
        public static final String TMGI = "org.mcopenplatform.muoapi.ConstantsMCOP.EMBMSNotificationEventExtras" + ".TMGI";

        /**
         * <h2>Key Access to the session ID. This identifier is unique for each call.</h2>
         * <p>Response: String</p>
         * <p>The eMBMS event types in which this tag can be present are: </br> </p>
         * <p>EMBMSEventInCoverage {@link EMBMSNotificationEventTypeEnum#EMBMSEventInCoverage}</br> </p>
         * <p>EMBMSEventInUse {@link EMBMSNotificationEventTypeEnum#EMBMSEventInUse}</br> </p>
         * <p>EMBMSEventNotInUse {@link EMBMSNotificationEventTypeEnum#EMBMSEventNotInUse}</br> </p>
         * <p>EMBMSEventOutCoverage {@link EMBMSNotificationEventTypeEnum#EMBMSEventOutCoverage}</p>
         */
        public static final String SESSION_ID = "org.mcopenplatform.muoapi.ConstantsMCOP.EMBMSNotificationEventExtras" + ".SESSION_ID";

        /**
         * <h2>Key Access to the list of eMBMS service areas.</h2>
         * <p>Response: String containing all the eMBMS service area identifiers consecutively.</p>
         * <p>The eMBMS event types in which this tag can be present are: </br> </p>
         * <p>EMBMSEventInCoverage {@link EMBMSNotificationEventTypeEnum#EMBMSEventInCoverage}</br> </p>
         * <p>EMBMSEventInUse {@link EMBMSNotificationEventTypeEnum#EMBMSEventInUse}</br> </p>
         * <p>EMBMSEventNotInUse {@link EMBMSNotificationEventTypeEnum#EMBMSEventNotInUse}</br> </p>
         * <p>EMBMSEventOutCoverage {@link EMBMSNotificationEventTypeEnum#EMBMSEventOutCoverage}</p>
         */
        public static final String AREA_LIST = "org.mcopenplatform.muoapi.ConstantsMCOP.EMBMSNotificationEventExtras" + ".AREA_LIST";

        /**
         * <h2>Key Access to the error codes associated to the eMBMS notification events.</h2>
         * <p>Response: Integer indicating the error code.</p>
         * <p>The eMBMS notification event types in which this tag can be present are: </br> </p>
         * <p>EMBMSEventError {@link org.mcopenplatform.muoapi.ConstantsMCOP.EMBMSNotificationEventExtras.EMBMSNotificationEventTypeEnum#EMBMSEventError}</p>
         * <p>Values:</p>
         * <p>0 means no errors.</p>
         *
         */
        public static final String ERROR_CODE = "org.mcopenplatform.muoapi.ConstantsMCOP.EMBMSNotificationEventExtras" + ".ERROR_CODE";

        /**
         * <h2>Key Access to error string associated to the eMBMS notification events.</h2>
         * <p>Response: String describing the error.</p>
         */
        public static final String ERROR_STRING = "org.mcopenplatform.muoapi.ConstantsMCOP.EMBMSNotificationEventExtras" + ".ERROR_STRING";

        /**
         * @enum EMBMSNotificationEventTypeEnum
         * @brief Enumeration of all possible eMBMS event types
         *
         * <h2>Defines the different events that can occur related to eMBMS procedures.</h2>
         */
        public enum EMBMSNotificationEventTypeEnum {
            EMBMSEventNone(0x00), /*!< Invalid eMBMS event */
            EMBMSEventError(0x01), /*!< eMBMS error report event */
            EMBMSEventAvailable(0x02), /*!< eMBMS availability report in a certain area */
            EMBMSEventInCoverage(0x03), /*!< The UE is under eMBMS coverage */
            EMBMSEventInUse(0x04), /*!< The UE is using a particular eMBMS bearer for media transmissions */
            EMBMSEventNotInUse(0x05), /*!< The UE is not using eMBMS to transmit media */
            EMBMSEventOutCoverage(0x06), /*!< The UE is out of eMBMS coverage */
            EMBMSEventNotAvailable(0x07); /*!< eMBMS is not available anymore in a certain area*/
            private int code;

            EMBMSNotificationEventTypeEnum(int code) {
                this.code = code;
            }

            public int getValue() {
                return code;
            }

            private static Map map = new HashMap<>();

            static {
                for (EMBMSNotificationEventTypeEnum pageType : EMBMSNotificationEventTypeEnum.values()) {
                    map.put(pageType.code, pageType);
                }
            }

            public static EMBMSNotificationEventTypeEnum fromInt(int pageType) {
                return (EMBMSNotificationEventTypeEnum) map.get(pageType);
            }
        }
    }

    /**
     * @enum ActionsCallBack
     * @brief Structure for the definition of the different callback events.
     *
     */
    public enum ActionsCallBack {
        none(""),
        registrationEvent("org.mcopenplatform.muoapi.ConstantsMCOP.ActionsCallBack.registrationEvent"),
        callEvent("org.mcopenplatform.muoapi.ConstantsMCOP.ActionsCallBack.callEvent"),
        controlMechanismEvent("org.mcopenplatform.muoapi.ConstantsMCOP.ActionsCallBack.controlMechanismEvent"),
        mcdataEvent("org.mcopenplatform.muoapi.ConstantsMCOP.ActionsCallBack.mcdataEvent"),
        emergencyAlertEvent("org.mcopenplatform.muoapi.ConstantsMCOP.ActionsCallBack.emergencyAlertEvent"),
        configurationInfoEvent("org.mcopenplatform.muoapi.ConstantsMCOP.ActionsCallBack.configurationInfoEvent"),
        groupInfoEvent("org.mcopenplatform.muoapi.ConstantsMCOP.ActionsCallBack.groupInfoEvent"),
        groupAffiliationEvent("org.mcopenplatform.muoapi.ConstantsMCOP.ActionsCallBack.groupAffiliationEvent"),
        selectedContactChangeEvent("org.mcopenplatform.muoapi.ConstantsMCOP.ActionsCallBack.selectedContactChangeEvent"),
        functionalAliasEvent("org.mcopenplatform.muoapi.ConstantsMCOP.ActionsCallBack.functionalAliasEvent"),
        eMBMSNotificationEvent("org.mcopenplatform.muoapi.ConstantsMCOP.ActionsCallBack.eMBMSNotificationEvent");

        private final String text;

        /**
         * @param text
         */
        ActionsCallBack(final String text) {
            this.text = text;
        }

        /* (non-Javadoc)
         * @see java.lang.Enum#toString()
         */
        @Override
        public String toString() {
            return text;
        }

        /**
         * @return the Enum representation for the given string.
         */
        public static ActionsCallBack fromString(String s) {
            for (ActionsCallBack actionsCallBack : ActionsCallBack.values()) {
                if (actionsCallBack.toString().compareTo(s) == 0) {
                    return actionsCallBack;
                }
            }
            return null;
        }
    }

    /**
     * @enum EmergencyTypeEnum
     * @brief Enumeration of all possible group status change options
     *
     * <h2>Defines the different event types for method {@link org.mcopenplatform.muoapi.IMCOPsdk#updateCallStatus(String, int)} </h2>
     */
    public enum EmergencyTypeEnum {
        NONE(0x00), /*!< Invalid call status update request type */
        EMERGENCY(0x01), /*!< Emergency priority call status update request type */
        NO_EMERGENCY(0x02), /*!< Normal priority call status update request type */
        BROADCAST(0x03), /*!< Broadcast call update request type */
        NO_BROADCAST(0x04), /*!< Prearranged call update request type */
        IMMINENT_PERIL(0x05); /*!< Imminent peril priority call status update request type */

        private int code;

        EmergencyTypeEnum(int code) {
            this.code = code;
        }

        public int getValue() {
            return code;
        }

        private static Map map = new HashMap<>();

        static {
            for (EmergencyTypeEnum pageType : EmergencyTypeEnum.values()) {
                map.put(pageType.code, pageType);
            }
        }

        public static EmergencyTypeEnum fromInt(int pageType) {
            return (EmergencyTypeEnum) map.get(pageType);
        }
    }

    /**
     * @enum ServiceTypeEnum
     * @brief Enumeration of all possible MCX Service types
     *
     * <h2>Defines the different services in MCX</h2>
     */
    public enum ServiceTypeEnum {
        ServiceNone(0), /*!< Invalid MCS Service type */
        //MCPTT
        ServiceMCPTT(1), /*!< MCPTT Service type */
        ServiceMCVideo(2), /*!< MCVideo Service type */
        ServiceMCData(3); /*!< MCData Service type */

        private int code;

        ServiceTypeEnum(int code) {
            this.code = code;
        }

        public int getValue() {
            return code;
        }

        private static Map map = new HashMap<>();

        static {
            for (ServiceTypeEnum pageType : ServiceTypeEnum.values()) {
                map.put(pageType.code, pageType);
            }
        }

        public static ServiceTypeEnum fromInt(int pageType) {
            return (ServiceTypeEnum) map.get(pageType);
        }
    }
}